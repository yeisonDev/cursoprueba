package com.example.cesdestore;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText etId,etProducto,etValor,etCategoria,etDescripcion;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etId= findViewById(R.id.etId);
        etProducto= findViewById(R.id.etProducto);
        etValor= findViewById(R.id.etValor);
        etCategoria= findViewById(R.id.etCategoria);
        etDescripcion= findViewById(R.id.etDescripcion);

    }

    private void servicioHttp(String URL){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MainActivity.this, "Operacion Correcta" + response, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, String.valueOf(error) , Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parametros= new HashMap<>();
                parametros.put("id", etId.getText().toString());
                parametros.put("producto", etProducto.getText().toString());
                parametros.put("valor", etValor.getText().toString());
                parametros.put("categoria", etCategoria.getText().toString());
                parametros.put("descripcion", etDescripcion.getText().toString());
                return parametros;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }

    public void insertarProducto(View view) {
        servicioHttp("http://172.16.59.239:81/proyectoweb/nuevo.php");
       // http://localhost:81/proyectoweb/nuevo.php
       //servicioHttp("localhost:81/proyectoweb/nuevo.php");
    }
}
