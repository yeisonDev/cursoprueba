package com.guzman.yeison.domiciliosyeison.DataBAse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.guzman.yeison.domiciliosyeison.Entities.Pedido;

import java.util.ArrayList;

public class PedidoBD {
    private PedidoSQLhelper sqliteHelper;
    private SQLiteDatabase database;

    public PedidoBD(Context context ){
        sqliteHelper=new PedidoSQLhelper(context);
        database=sqliteHelper.getWritableDatabase();

    }

    public boolean InsertarPedido(Pedido pedido){

        ContentValues Values= new ContentValues();

        Values.put("Pedido",pedido.getProductoPedido());
        Values.put("Local",pedido.getLocalPedido());
        Values.put("Precio",pedido.getPreciop());
        Values.put("Nombre",pedido.getNombrePedido());
        Values.put("Direccion",pedido.getDireccionP());
        Values.put("Telefono",pedido.getTelefonop());

        long resultado=database.insert("Pedido",null,Values);
        return resultado > 0;

    }

    public ArrayList<Pedido> ObtenerPedido(){

        ArrayList<Pedido> Pedidos=new ArrayList<>();

        String query= "SELECT * FROM Pedido";

        Cursor registros=database.rawQuery(query,null);

        while (registros.moveToNext()){

            Pedido pedido= new Pedido();
            pedido.setId(registros.getInt(0));
            pedido.setProductoPedido(registros.getString(1));
            pedido.setLocalPedido(registros.getString(2));
            pedido.setPreciop(registros.getString(3));
            pedido.setNombrePedido(registros.getString(4));
            pedido.setDireccionP(registros.getString(5));
            pedido.setTelefonop(registros.getString(6));

            Pedidos.add(pedido);
        }


        return Pedidos;
    }

}
