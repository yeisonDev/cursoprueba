package com.guzman.yeison.domiciliosyeison.DataBAse;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PedidoSQLhelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="pedidosbd";
    private static final int DATABASE_VERSION= 1;

    public PedidoSQLhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tablaPedido= "CREATE TABLE Pedido\n" +
                "(\n" +
                " Id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                " Pedido TEXT,\n" +
                " Local  TEXT,\n" +
                " Precio TEXT,\n" +
                " Nombre TEXT,\n" +
                " Direccion TEXT,\n" +
                " Telefono TEXT\n" +
                ")";
        db.execSQL(tablaPedido);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Pedido");

    }
}
