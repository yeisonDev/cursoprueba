package com.guzman.yeison.domiciliosyeison.DataBAse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RegistroBD extends SQLiteOpenHelper {

    public RegistroBD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query="create table usuario(Id integer primary key autoincrement, Nombre text, Email text, Contraseña text)";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //METODO PARA ABRIR LA BASE DE DATOS
    public void abrir (){
        this.getWritableDatabase();
    }

    //METODO PARA CERRAR LA BASE DE DATOS
    public void cerrar(){
        this.close();
    }

    //METODO QUE PERMITE INSERTAR REGISTRO EN LA TABLA USUARIOS
    public void insertarReg(){
        ContentValues valores = new ContentValues();
        valores.put("Nombre","yeison");
        valores.put("Email","yeison15");
        valores.put("Contraseña","12345");
        this.getWritableDatabase().insert("usuario", null, valores);
    }

    //METODO QUE PERMITE VALIDAR SI EL USUARIO EXISTE
    public Cursor ConsultarUsuario(String usu, String pas) throws SQLException {
        Cursor mcursor = null;
        mcursor = this.getReadableDatabase().query("usuario", new String[]{"Id","Nombre","Email","Contraseña"},"Email like " +
                "'"+usu+"' and Contraseña like '"+pas+"'",null, null, null, null);
        return mcursor;
    }
}
