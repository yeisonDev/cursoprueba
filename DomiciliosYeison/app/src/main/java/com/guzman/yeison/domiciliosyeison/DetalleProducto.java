package com.guzman.yeison.domiciliosyeison;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetalleProducto extends AppCompatActivity {

    Button btnpedir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_producto);

        Bundle params= getIntent().getExtras();
        String Nombre=params.getString("pnombre");
        String Local=params.getString("plocal");
        String Precio=params.getString("pprecio");


        TextView tvNombre=findViewById(R.id.tvNombre);
        TextView tvLocal=findViewById(R.id.tvLocal);
        TextView tvprecio=findViewById(R.id.tvPrecio);

        tvNombre.setText(Nombre);
        tvLocal.setText(Local);
        tvprecio.setText(Precio);



        btnpedir=findViewById(R.id.btnpedir);


        btnpedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetalleProducto.this, NuevoPedido.class);
                startActivity(intent);

            }
        });



    }
}
