package com.guzman.yeison.domiciliosyeison.Entities;

public class Pedido {

    public Pedido(){};


    public Pedido(int Id, String ProductoPedido, String LocalPedido, String Precio, String NombrePedido, String DireccionP, String telefonop){
        this.setId(Id);
        this.setProductoPedido(ProductoPedido);
        this.setLocalPedido(LocalPedido);
        this.setPreciop(Precio);
        this.setNombrePedido(NombrePedido);
        this. setDireccionP(DireccionP);
        this.setTelefonop(telefonop);
    }
    private int Id;
    private String ProductoPedido;
    private  String LocalPedido;
    private  String Preciop;
    private String NombrePedido;
    private String DireccionP;
    private String telefonop;


    public String getProductoPedido() {
        return ProductoPedido;
    }

    public void setProductoPedido(String productoPedido) {
        ProductoPedido = productoPedido;
    }

    public String getLocalPedido() {
        return LocalPedido;
    }

    public void setLocalPedido(String localPedido) {
        LocalPedido = localPedido;
    }

    public String getPreciop() {
        return Preciop;
    }

    public void setPreciop(String preciop) {
        Preciop = preciop;
    }


    public String getNombrePedido() {
        return NombrePedido;
    }

    public void setNombrePedido(String nombrePedido) {
        NombrePedido = nombrePedido;
    }

    public String getDireccionP() {
        return DireccionP;
    }

    public void setDireccionP(String direccionP) {
        DireccionP = direccionP;
    }

    public String getTelefonop() {
        return telefonop;
    }

    public void setTelefonop(String telefonop) {
        this.telefonop = telefonop;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
