package com.guzman.yeison.domiciliosyeison.Entities;

public class Producto {

    public Producto(String nombre, String local, String precio)
    {
        this.nombres= nombre;
        this.local= local;
        this.precio= precio;
    }

    private String nombres;
    private String local;
    private String precio;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
