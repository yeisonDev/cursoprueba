package com.guzman.yeison.domiciliosyeison;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.guzman.yeison.domiciliosyeison.Entities.Producto;

import java.util.ArrayList;

public class ListaProductos extends AppCompatActivity {
    ArrayList<Producto> ListaMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_productos);
        CargarMenu();
        ArrayList<String> nombreMenu= new ArrayList<>();

        for (Producto producto:ListaMenu )
        {
            nombreMenu.add(producto.getNombres());
        }

        ListView lvproductos= findViewById(R.id.lvproductos);
        lvproductos.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,nombreMenu ));

        final Context main= this;
        lvproductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

                Producto seleccionado= ListaMenu.get(i);
                Intent detalle= new Intent(main,DetalleProducto.class);
                detalle.putExtra("pnombre",seleccionado.getNombres());
                detalle.putExtra("plocal",seleccionado.getLocal());
                detalle.putExtra("pprecio",seleccionado.getPrecio());
                startActivity(detalle);
            }
        });

    }
    private void CargarMenu(){

        ListaMenu =new ArrayList<Producto>();
        ListaMenu.add(new Producto("Hamburguesa","Comi Ricas","5000" ));
        ListaMenu.add(new Producto("Hamburguesa Especial","Comi Ricas","8000"));
        ListaMenu.add(new Producto("Perro ","Comi Ricas","4000" ));
        ListaMenu.add(new Producto("Perro Especial ","Comi Ricas","6000"));
        ListaMenu.add(new Producto("Perra ","Comi Ricas","6000"));
        ListaMenu.add(new Producto("Perra Especial ","Comi Ricas","9000"));
        ListaMenu.add(new Producto("Carne ","Comi Ricas","15000"));
    }
}
