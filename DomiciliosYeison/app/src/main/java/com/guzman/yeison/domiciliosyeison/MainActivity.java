package com.guzman.yeison.domiciliosyeison;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.guzman.yeison.domiciliosyeison.DataBAse.RegistroBD;

public class MainActivity extends AppCompatActivity {

   EditText etusuario;
   EditText etclave;
   Button btnregistro,btnnuregistro;

   RegistroBD helper= new RegistroBD(this,"BD1",null,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etusuario=findViewById(R.id.etusuario);
        etclave=findViewById(R.id.etclave);
        btnregistro=findViewById(R.id.btnregistro);


        btnregistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = helper.ConsultarUsuario(etusuario.getText().toString(), etclave.getText().toString());

                if (cursor.getCount()>0){
                    Intent i= new Intent(getApplicationContext(),MenuActivity.class);
                    startActivity(i);
                }else {
                    Toast.makeText(getApplicationContext(),"Usuario y/o Contraseña Incorrectos",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

}
