package com.guzman.yeison.domiciliosyeison;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {
    ImageView ivProductos;
    ImageView ivpedidos;
    ImageView ivsugerencia;
    Button btnsalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ivProductos= findViewById(R.id.ivProductos);
        ivpedidos=findViewById(R.id.ivPedidos);
        ivsugerencia= findViewById(R.id.ivSugerencias);
        btnsalir=findViewById(R.id.btnsalir);

        ivProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ListaProductos.class);
                startActivity(intent);

            }
        });

        ivpedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, MisPedidosActivity.class);
                startActivity(intent);
            }
        });

        ivsugerencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, Sugerencia.class);
                startActivity(intent);

            }
        });

        btnsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });

    }

}
