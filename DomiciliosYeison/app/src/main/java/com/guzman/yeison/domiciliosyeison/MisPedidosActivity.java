package com.guzman.yeison.domiciliosyeison;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.guzman.yeison.domiciliosyeison.DataBAse.PedidoBD;
import com.guzman.yeison.domiciliosyeison.Entities.Pedido;

import java.util.ArrayList;

public class MisPedidosActivity extends AppCompatActivity {
    ArrayList<Pedido> Listapedidos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_pedidos);
        cargarPedido();
        ArrayList<String> nombrePedido= new ArrayList<>();

        for (Pedido pedido:Listapedidos ){
            nombrePedido.add(pedido.getLocalPedido());
        }

        ListView lvpedidos=findViewById(R.id.lvpedidos);
        lvpedidos.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, nombrePedido));


    }

    public void cargarPedido(){

        PedidoBD bd= new PedidoBD(this);
        Listapedidos=bd.ObtenerPedido();
    }


}
