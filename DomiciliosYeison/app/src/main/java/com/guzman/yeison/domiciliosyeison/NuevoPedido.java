package com.guzman.yeison.domiciliosyeison;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.guzman.yeison.domiciliosyeison.DataBAse.PedidoBD;
import com.guzman.yeison.domiciliosyeison.Entities.Pedido;

public class NuevoPedido extends AppCompatActivity {

    EditText etSolicitudn;
    EditText etLocaln;
    EditText etValorn;
    EditText etSolicitanten;
    EditText etDireccionn;
    EditText etTelefon;
    Button btGuardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_pedido);


        etSolicitudn=findViewById(R.id.etSolicitudn);
        etLocaln=findViewById(R.id.etLocaln);
        etValorn=findViewById(R.id.etValorn);
        etSolicitanten=findViewById(R.id.etSolicitanten);
        etDireccionn=findViewById(R.id.etDireccionn);
        etTelefon=findViewById(R.id.etTelefon);
        btGuardar=findViewById(R.id.btGuardar);


        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Pedido pedido= new Pedido();
                pedido.setNombrePedido(etSolicitudn.getText().toString());
                pedido.setLocalPedido(etLocaln.getText().toString());
                pedido.setPreciop(etValorn.getText().toString());
                pedido.setNombrePedido(etSolicitanten.getText().toString());
                pedido.setDireccionP(etDireccionn.getText().toString());
                pedido.setTelefonop(etTelefon.getText().toString());

                PedidoBD pedidoBD=new PedidoBD(NuevoPedido.this);
                boolean respuesta =pedidoBD.InsertarPedido(pedido);
                if (respuesta){
                    Toast.makeText(NuevoPedido.this, "El Pedido se ha insertado correctamente!", Toast.LENGTH_LONG).show();

                }else {

                    Toast.makeText(NuevoPedido.this, "Ha ocurrido un error guardando el Pedido", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    public static class DetalleProduct {
    }
}
