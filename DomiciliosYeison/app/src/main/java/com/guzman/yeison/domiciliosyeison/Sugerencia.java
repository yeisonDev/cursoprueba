package com.guzman.yeison.domiciliosyeison;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Sugerencia extends AppCompatActivity {

    EditText etCorreo;
    EditText etAsunto;
    EditText etSugerencia;
    Button btEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugerencia);
        etCorreo= findViewById(R.id.etCorreo);
        etAsunto=findViewById(R.id.etAsunto);
        etSugerencia=findViewById(R.id.etSugerencia);
        btEnviar=findViewById(R.id.btEnviar);


        btEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etCorreo.getText().toString();
                String asunto=etAsunto.getText().toString();
                String sugerencia= etSugerencia.getText().toString();
                String[] emails = new String[1];
                emails[0] = email;
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, emails);
                intent.putExtra(Intent.EXTRA_SUBJECT,asunto);
                intent.putExtra(Intent.EXTRA_TEXT, sugerencia);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });



    }
}
