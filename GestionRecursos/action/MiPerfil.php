
<style type="text/css" media="screen">
	
	.perfil {
       width: 100%;
        display: block;
        border: 10px double;
        border-radius: 30px;

	}

#formulario{
	 width: 365px;
    background: #A9A9A9;
    padding: 10px;
    border: 5px solid #2F4F4F;
    border-radius: 10px
}

</style>
<?php
session_start();
require "sessiones.php";

$titulo= "Yeison | Perfil";
 // Defimos el valor de mi js 
 $js_files= array("<script src='../vistas/js/app/miPerfil.js'></script>");?>


<!-- 	incluimos el menu  -->
<?php require_once '../vistas/header.php';?>

  <div class="right_col" role="main" ng-controller="miperfil as perfil" > <!-- page content -->
                <!-- content -->
                <br><br>
                <div class="row" ng-init="perfil.inicio()">
                    <div class="col-md-4">
                        <div class="image view view-first">
                            <?php if(!isset($_SESSION['foto'])){?>
                                <img class="thumb-image perfil" id="imagen" src="../vistas/assets/img/avatar3.png" alt="image" />
                            <?php } else {;?>
                            <img class="thumb-image perfil" id="imagen" src="../vistas/assets/img/<?php echo $_SESSION['foto'];$_SESSION['foto']?>" alt="image"/>
                             <?php };?>
                            <br>
                        </div>
                            <form method="post" id="formulario"  enctype="multipart/form-data">
                            <b> Nombre:</b> {{perfil.usuario.nombre + " " + perfil.usuario.apellido }} <br>
                             <b>Correo:</b> {{perfil.usuario.correo}}
                               <!--  <input type="file" file-input="files"/>
                            <button  type="button" class="btn btn-info" ng-click="cambiarfoto()">Cambiar imagen</button> -->
                            </form>
                        <div id="respuesta">
                            <a href="perfilFoto.php">Cambiar foto de perfil </a>
                        </div>
                    </div>
                   <div	class="col-md-8">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Informacion personal </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                            <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <br />

                                <form id="demo-form2" novalidate class="form-horizontal form-label-left" method="post" name="myForm" >
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="name" id="first-name" ng-model= "perfil.usuario.nombre" class="form-control col-md-7 col-xs-12"  required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Apellido
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="name" id="first-name" ng-model= "perfil.usuario.apellido" class="form-control col-md-7 col-xs-12" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Correo electronico 
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="last-name" name="email" ng-model= "perfil.usuario.correo" class="form-control col-md-7 col-xs-12" required>
                                        </div>
                                    </div>

                                    <br><br><br>
                                    <div class="row">     
                                    <h2 class="col-md-7" style="padding-left: 80px;">Cambiar Contraseña</h2>
                                     <input type="checkbox" style="margin-top: 30px;" ng-model="cambiarCon"> 
                                    </div>          
                                    <div ng-if="cambiarCon">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña antigua
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="birthday" name="password" class="date-picker form-control col-md-7 col-xs-12" type="password" placeholder="**********" ng-model="perfil.contra.passA" minlength="1">
                                        </div>
                                      
                                    </div>
                                                                         
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nueva contraseña 
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="birthday" name="new_password" class="date-picker form-control col-md-7 col-xs-12" type="password" placeholder="**********" ng-model="perfil.contra.passN" ng-disabled="myForm.password.$pristine||myForm.password.$invalid" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar contraseña nueva
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="birthday" name="confirm_new_password" class="date-picker form-control col-md-7 col-xs-12" type="password" placeholder="**********" ng-model="perfil.contra.passC" ng-disabled="myForm.password.$pristine||myForm.password.$invalid" equal="perfil.contra.passN">
                                        </div>
                                    </div>
                                     <div class="alert alert-info col-md-6" style="border-radius:20px" ng-show="myForm.confirm_new_password.$invalid"> 
                                            <strong>Alerta!!</strong> Confirmar la misma contraseña
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="button" name="token"  class="btn btn-success" ng-click="perfil.actualizarDatos()" style="color: white;">Guardar datos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                  </div>
             </div>
               </div>
          </div>
    </div><!-- /page content -->


<?php require_once '../vistas/footer.php';?>
