<?php

session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cambiar foto de perfil</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../vistas/css/main.css">
    <link rel="stylesheet" href="../vistas/css/sweetalert2.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body >
<nav class="navbar navbar-dark bg-dark">
    <h3>Bienvenido! selecciona tu foto de perfil</h3>
</nav>

<div class="col-md-5">
</div>
<div class="container">

    <div class="row">
        <div class="card" style="width: 18rem; border:10px double;" >
            <img src="../vistas/assets/img/<?php echo $_SESSION['foto'];?>" class="card-img-top" alt="..." >
            <div class="card-body">
                <p class="card-text">Selecciona tu nueva foto de perfil</p>
        </div>
            <form class="form-signin" method="post" enctype="multipart/form-data" action="../controladores/cambiarFotoControlador.php">
                <div class="image view view-first">
                    <input type="file" id="inputPassword" name="file" style="width: 154px; margin-left: 10PX;">
                    <button class="btn btn-lg btn-primary btn-block" type="submit" style="color: white">Cambiar foto</button>
            </form>
</div>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
</body>
</html>