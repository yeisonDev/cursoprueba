
<!-- Definimos el titulo -->
<?php 

  require "sessiones.php";
$titulo= "Yeison | grupo";
 // Defimos el valor de mi js 
$js_files= array("<script src='../vistas/js/app/home.js'></script>");?>
<!-- 	incluimos el menu  -->
<?php require_once '../vistas/header.php';?>

	<div class="container-fluid">
			<div class="page-header">
			  <h1 class="text-titles">Sistema de Gestión <small> Bienvenido...</small></h1>
			</div>
		</div>
		<div class="full-box text-center" style="padding: 30px 10px;" ng-controller="homeCtrl as home">
            <div ng-init="home.cargaDatos()">
			<article class="full-box tile">
				<div class="full-box tile-title text-center text-titles text-uppercase">
					Administracion
				</div>
				<div class="full-box tile-icon text-center">
					<i class="zmdi zmdi-account"></i>
				</div>
				<div class="full-box tile-number text-titles">
					<p class="full-box">{{home.admin}}</p>
					<small>Area Administrativa</small>
				</div>
			</article>
			<article class="full-box tile">
				<div class="full-box tile-title text-center text-titles text-uppercase">
					OPerativa
				</div>
				<div class="full-box tile-icon text-center">
					<i class="zmdi zmdi-male-alt"></i>
				</div>
				<div class="full-box tile-number text-titles">
					<p class="full-box">{{home.usu}}</p>
					<small>Area de operaciónes</small>
				</div>
			</article>
			<article class="full-box tile">
				<div class="full-box tile-title text-center text-titles text-uppercase">
					Contabilidad
				</div>
				<div class="full-box tile-icon text-center">
					<i class="zmdi zmdi-face"></i>
				</div>
				<div class="full-box tile-number text-titles" >
					<p class="full-box">{{home.conta}}</p>
					<small>Area de contabilidad</small>
				</div>
			</article>
			<article class="full-box tile">
				<div class="full-box tile-title text-center text-titles text-uppercase">
					Tecnología
				</div>
				<div class="full-box tile-icon text-center">
					<i class="zmdi zmdi-male-female"></i>
				</div>
				<div class="full-box tile-number text-titles">
					<p class="full-box">{{home.tecno}}</p>
					<small>Area de Tecnología</small>
				</div>
			</article>
		  </div>
        </div>

<!-- Incluimos el los scripst y el pie de pagina -->
<?php require_once '../vistas/footer.php';?>

