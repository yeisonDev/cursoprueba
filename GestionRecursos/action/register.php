<!-- Definimos el titulo -->
<?php

require "sessiones.php";
$titulo = "Yeison | grupo";
// Defimos el valor de mi js
$js_files = array("<script src='../vistas/js/app/register.js'></script>"); ?>
<!-- 	incluimos el menu  -->
<?php require_once '../vistas/header.php'; ?>

<div class="container justify-content-center"
     ng-controller="registerCtrl as ctrl">
    <div class="card bg-dark">
        <div class="card-header bg-dark text-white">Registro</div>
        <div class="card-body">
            <form name="form" ng-submit="ctrl.newRegist()">
                <div class="form-row">
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation01">Primer Nombre *</label>
                        <input type="text" name="validation01"
                               class="form-control"
                               ng-class="{'is-valid': form.validation01.$dirty&&form.validation01.$valid,
                       'is-invalid':form.validation01.$dirty&&form.validation01.$invalid }"
                               ng-model="ctrl.register.firstName"
                               id="validationServer01"
                               only-letters-input required>

                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation02">Segundo Nombre </label>
                        <input type="text" name="validation02"
                               class="form-control" id="validation02"
                               ng-class="{'is-valid': form.validation02.$dirty&&form.validation02.$valid,
                       'is-invalid':form.validation02.$dirty&&form.validation02.$invalid }"
                               ng-model="ctrl.register.secondName"
                               only-letters-input>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation03">Primer Apellido *</label>
                            <input type="text" name="validation03"
                                   class="form-control" id="validation03"
                                   placeholder="Username"
                                   aria-describedby="inputGroupPrepend3"
                                   ng-class="{'is-valid': form.validation03.$dirty &&form.validation03.$valid,
                       'is-invalid':form.validation03.$dirty&&form.validation03.$invalid }"
                                   only-letters-input
                                   ng-model="ctrl.register.lastName" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validatio03">Segundo apellido</label>
                        <input type="text" class="form-control"
                               name="validatio03" id="validatio03" ng-class="{'is-valid': form.validatio03.$dirty &&form.validatio03.$valid,
                       'is-invalid':form.validatio03.$dirty&&form.validatio03.$invalid }"
                               only-letters-input
                               ng-model="ctrl.register.lastNameTwo">

                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation04">Tipo de documento *</label>
                        <select class="form-control" name="validation04"
                                id="validation04" ng-class="{'is-valid': form.validation04.$dirty &&form.validation04.$valid,
                       'is-invalid':form.validation04.$dirty&&form.validation04.$invalid }"
                                ng-model="ctrl.register.typeId" required>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                    </div>

                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation18">Numero de identificación
                            *</label>
                            <input type="text" name="validation18"
                                   class="form-control " id="validation18"
                                   aria-describedby="inputGroupPrepend3"
                                   ng-class="{'is-valid': form.validation18.$dirty &&form.validation18.$valid,
                       'is-invalid':form.validation18.$dirty&&form.validation18.$invalid }"
                                   numbers-only
                                   ng-maxlength="12" minlength="6"
                                   ng-model="ctrl.register.numId" required>
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation17">Fecha de expedicion *</label>
                        <input type="date" class="form-control"
                               id="validation17" name="validavalidation17tion11"
                               ng-class="{'is-valid': form.validation11.$dirty &&form.validation11.$valid,
                       'is-invalid':form.validation17.$dirty&&form.validation17.$invalid }"
                               ng-model="ctrl.register.dateExp"  date-picker required>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation05">Correo electrónico *</label>
                        <input type="email" class="form-control"
                               id="validation05" name="validation05" ng-class="{'is-valid': form.validation05.$dirty &&form.validation05.$valid,
                       'is-invalid':form.validation05.$dirty&&form.validation05.$invalid }"
                               pattern="^[-.@_A-z0-9]*$"
                               ng-model="ctrl.register.email" required>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation10">Confirmar Correo elecronico
                            *</label>
                        <input type="email" class="form-control"
                               id="validation10" name="validation10" ng-class="{'is-valid': form.validation10.$dirty &&form.validation10.$valid,
                       'is-invalid':form.validation10.$dirty&&form.validation10.$invalid }"
                               equal="ctrl.register.email"
                               pattern="^[-.@_A-z0-9]*$"
                               ng-model="ctrl.register.emailConfirm"
                               ng-paste="$event.preventDefault()" required>
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation11">Contraseña *</label>
                        <input type="password" class="form-control"
                               id="validation11" name="validation11" ng-class="{'is-valid': form.validation11.$dirty &&form.validation11.$valid,
                       'is-invalid':form.validation11.$dirty&&form.validation11.$invalid }"
                               ng-pattern="/(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-zA-Z]).*$/"
                               ng-model="ctrl.register.pass" required>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation08">Confirmar Contraseña *</label>
                        <input type="password" class="form-control"
                               id="validation08" name="validation08" ng-class="{'is-valid': form.validation08.$dirty &&form.validation08.$valid,
                       'is-invalid':form.validation08.$dirty&&form.validation08.$invalid }"
                               ng-paste="$event.preventDefault()"
                               equal="ctrl.register.pass"
                               ng-pattern="/(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[a-zA-Z]).*$/"
                               ng-model="ctrl.register.passConfirm" required>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation16">Telefono fijo *</label>
                        <input type="password" class="form-control"
                               id="validation16" name="validation16" ng-class="{'is-valid': form.validation16.$dirty &&form.validation16.$valid,
                       'is-invalid':form.validation16.$dirty&&form.validation16.$invalid }"
                               numbers-only
                               ng-model="ctrl.register.phone" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation20">Celular *</label>
                        <input type="text" class="form-control"
                               id="validation20" name="validation20" ng-class="{'is-valid': form.validation20.$dirty &&form.validation20.$valid,
                       'is-invalid':form.validation20.$dirty&&form.validation20.$invalid }"
                               numbers-only
                               ng-model="ctrl.register.celPhone" required>
                    </div>
                    <div class="col-xs-12 col-md-4 mb-3">
                        <label for="validation24">Sexo *</label>
                        <select name="validation24" class="form-control"
                                id="validation24" ng-class="{'is-valid': form.validation24.$dirty &&form.validation24.$valid,
                       'is-invalid':form.validation24.$dirty&&form.validation24.$invalid }"
                                ng-model="ctrl.register.sex" required>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>
                </div>
                <p>
                    <a style="color: #e6e6e6" data-toggle="collapse"
                       href="#collapseInfo" role="button"
                       aria-expanded="false"
                       aria-controls="collapseInfo">
                        Información Adicional
                    </a>
                </p>
                <div class="collapse" id="collapseInfo">
                    <div class="form-row">
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation15">Fecha de Nacimiento
                                *</label>
                            <input type="date" name="validation15"
                                   class="form-control" id="validation15"
                                   ng-class="{'is-valid': form.validation12.$dirty &&form.validation12.$valid,
                       'is-invalid':form.validation15.$dirty&&form.validation15.$invalid }"
                                   ng-model="ctrl.register.dateBirth" required>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validacion10">Departamento de nacimiento
                                *</label>
                            <select class="form-control" name="validacion10"
                                    id="validacion10" ng-class="{'is-valid': form.validacion10.$dirty &&form.validacion10.$valid,
                       'is-invalid':form.validacion10.$dirty&&form.validacion10.$invalid }"
                                    ng-model="ctrl.register.departBirth "
                                    required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validacion11">Ciudad de nacimiento
                                *</label>
                            <select name="validacion11" class="form-control"
                                    id="validacion11" ng-class="{'is-valid': form.validacion11.$dirty &&form.validacion11.$valid,
                       'is-invalid':form.validacion11.$dirty&&form.validacion11.$invalid }"
                                    ng-model="ctrl.register.cityBirth" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation12">Departamento de residencia
                                *</label>
                            <select name="validation12" class="form-control"
                                    id="validation12" ng-class="{'is-valid': form.validation12.$dirty &&form.validation12.$valid,
                       'is-invalid':form.validation12.$dirty&&form.validation12.$invalid }"
                                    ng-model="ctrl.register.departRes" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation13">Ciudad de residencia
                                *</label>
                            <select name="validation13" class="form-control"
                                    id="validation13" ng-class="{'is-valid': form.validation13.$dirty &&form.validation13.$valid,
                       'is-invalid':form.validation13.$dirty&&form.validation13.$invalid }"
                                    ng-model="ctrl.register.cityRes" required>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation21">Dirección *</label>
                            <input type="text" class="form-control"
                                   id="validation21" name="validation21"
                                   ng-class="{'is-valid': form.validation21.$dirty &&form.validation21.$valid,
                       'is-invalid':form.validation21.$dirty&&form.validation21.$invalid }"
                                   maxlength="150" ng-model="ctrl.register.address" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation23">Nacionalidad *</label>
                            <select name="validation23" class="form-control"
                                    id="validation23" ng-class="{'is-valid': form.validation23.$dirty &&form.validation23.$valid,
                       'is-invalid':form.validation13.$dirty&&form.validation13.$invalid }"
                                    ng-model="ctrl.register.nacionality"
                                    required>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation23">Departamento de expedicion
                                *</label>
                            <select name="validation23" class="form-control"
                                    id="validation12" ng-class="{'is-valid': form.validation23.$dirty &&form.validation23.$valid,
                       'is-invalid':form.validation23.$dirty&&form.validation23.$invalid }"
                                    ng-model="ctrl.register.departEx" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-4 mb-3">
                            <label for="validation24">Ciudad de expedicion
                                *</label>
                            <select name="validation24" class="form-control"
                                    id="validation24" ng-class="{'is-valid': form.validation24.$dirty &&form.validation24.$valid,
                       'is-invalid':form.validation24.$dirty&&form.validation24.$invalid }"
                                    ng-model="ctrl.register.cityEx" required>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input"
                                   id="customSwitch1"
                                   ng-model="ctrl.register.check" required>
                            <label class="custom-control-label"
                                   for="customSwitch1">Terminos y
                                condiciones</label>
                        </div>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input"
                                   id="customSwitch2"
                                   ng-model="ctrl.register.habe" required>
                            <label class="custom-control-label"
                                   for="customSwitch2">Habeas Data</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input style="color: #e6e6e6" class="btn btn-info"
                               type="submit"
                               ng-disabled="form.$dirty && form.$invalid"
                               value="Enviar">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require_once '../vistas/footer.php'; ?>
