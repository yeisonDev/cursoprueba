<?php
/*
Este include guardara la validacion de acceso al aplicativo que se incluira en cada pagina que lo necesite
*/
if (session_status() == PHP_SESSION_NONE) {
     session_start();
        }

if (!isset($_SESSION["usuario"])) {
	header("Location: ../index.php");
	exit();
}

