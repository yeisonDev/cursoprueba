<?php

require "sessiones.php";
$titulo= "Yeison | grupo";
// Defimos el valor de mi js
$js_files= array("<script src='../vistas/js/app/usuarioActualizar.js'></script>");?>
<!-- 	incluimos el menu  -->
<?php require_once '../vistas/header.php';?>

<div class="container-fluid">
    <div class="page-header">
        <h1 class="text-titles"><i class="zmdi zmdi-male-alt zmdi-hc-fw"></i> Administrar <small>Administrar Usuarios</small></h1>
    </div>
    <p class="lead">Bienvenido al sistema de gestion de usuarios!</p>
    <input type="hidden" id="id" value="<?php echo $_GET['id'];?>" >
</div>
<div class="container-fluid" ng-controller="usuarioActuCtrl as usuario">
    <div class="row" ng-init="usuario.cargaDatos()">
        <div class="col-xs-12">
            <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                <li class="active"><a href="#new" data-toggle="tab">Actualizar usuario</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane  active in" id="new">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-md-10 col-md-offset-1">
                                <form name="nuevo" ng-submit='usuario.actualizarUsuario()' >
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nombre</label>
                                        <input class="form-control" type="text" ng-model="usuario.u.nombre" maxlength="20" required>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Apellido</label>
                                        <input class="form-control" type="text" ng-model="usuario.u.apellido" maxlength="50" required>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Correo electrónico</label>
                                        <input class="form-control" type="email" ng-model="usuario.u.correo" maxlength="100" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Estado</label>
                                        <select class="form-control" ng-model="usuario.u.estado">
                                            <option value="">Seleccione...</option>
                                            <option value="1">Activo</option>
                                            <option value="0">Inactivo</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Salario</label>
                                        <select class="form-control" ng-model="usuario.u.salario" required>
                                            <option value="">Seleccione...</option>
                                            <option value="800000">800.000</option>
                                            <option value="1000000">1000.000</option>
                                            <option value="2000000">2000.000</option>
                                        </select>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telefono</label>
                                        <input class="form-control" numbers-only type="text" ng-model="usuario.u.tel" maxlength="7" required>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Celular</label>
                                        <input class="form-control" numbers-only type="text" ng-model="usuario.u.cel"  maxlength="11" required>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Direccion</label>
                                        <input class="form-control" type="text" ng-model="usuario.u.dir" maxlength="100" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Perfil</label>
                                        <select class="form-control" ng-model="usuario.u.perfil"
                                                ng-options="obj.perfil_id as obj.descripcion for obj in usuario.perfiles"  required>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Cargo</label>
                                        <select class="form-control" ng-model="usuario.u.cargo"
                                                ng-options="obj.idcargo as obj.cargo for obj in usuario.cargos"  required>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Eps</label>
                                        <select class="form-control" ng-model="usuario.u.eps"
                                                ng-options="obj.ideps as obj.eps for obj in usuario.epss"  required>
                                            <option value=''>Seleccione...</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Arl</label>
                                        <select class="form-control" ng-model="usuario.u.arl"
                                                ng-options="obj.idarl as obj.arl for obj in usuario.arls"  required>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                    <p class="text-center" >
                                        <button type="submit"  class="btn btn-info btn-raised btn-sm" ><i class="zmdi zmdi-floppy"></i> Actualizar</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Incluimos el los scripst y el pie de pagina -->
<?php require_once '../vistas/footer.php';?>
