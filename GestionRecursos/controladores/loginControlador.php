<?php
error_reporting(E_ALL);
//llamamos los encabezados

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');


//resibimos la información
$data = json_decode(file_get_contents("php://input"));

if (isset($data->metodo)) {

   switch ($data->metodo) {
      case 'login':
           require_once '../modelos/loginModelo.php';
          $datos= new LoginModelo();
          $datos->validarLogin($data->datos);
          break;
   }
 
}
