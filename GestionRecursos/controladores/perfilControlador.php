<?php
error_reporting(E_ALL);
//llamamos los encabezados

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');


//resibimos la información
$data = json_decode(file_get_contents("php://input"));
$data->correo= isset($data->correo)?$data->correo:"";
$data->datos1= isset($data->datos1)?$data->datos1:"";
$data->datos2= isset($data->datos2)?$data->datos2:"";

if (isset($data->metodo)) {
   switch ($data->metodo) {
      case 'traer':
          require_once '../modelos/perfilModel.php';
          $datos= new PerfilModel();
          $datos->traerDatos();
          break;

     case 'actualizarDatos':
          require_once '../modelos/perfilModel.php';
          $datos= new PerfilModel();
          $datos->actualizarDatos($data->datos1,$data->datos2);
          break;
   }
 
}
