<?php
error_reporting(E_ALL);
//llamamos los encabezados

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');


//resibimos la información
$data = json_decode(file_get_contents("php://input"));
$data->correo= isset($data->correo)?$data->correo:"";
$data->datos1= isset($data->datos1)?$data->datos1:"";
$data->datos2= isset($data->datos2)?$data->datos2:"";

if (isset($data->metodo)) {
    switch ($data->metodo) {
        case 'datosUsuario':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->datosUsuario($data->data);
            break;
        case 'actualizarUsuario':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->actualizarUsuario($data->data);
            break;
    }
}
