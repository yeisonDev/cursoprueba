<?php
error_reporting(E_ALL);
//llamamos los encabezados

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');


//resibimos la información
$data = json_decode(file_get_contents("php://input"));
$data->correo= isset($data->correo)?$data->correo:"";
$data->datos1= isset($data->datos1)?$data->datos1:"";
$data->datos2= isset($data->datos2)?$data->datos2:"";

if (isset($data->metodo)) {
    switch ($data->metodo) {
        case 'cargo':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->cargo();
            break;

        case 'arl':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->arl();
            break;

        case 'eps':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->eps();
            break;
        case 'perfil':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->perfil();
            break;
        case 'crearUsuario':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->crearUsuario($data->data);
            break;
        case 'tablero':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->tablero();
            break;
        case 'lista':
            require_once '../modelos/usuario.php';
            $datos= new usuario();
            $datos->listarUsuarios();
            break;
    }

}
