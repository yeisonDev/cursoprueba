<?php require_once "core/configGeneral.php"?>

<!DOCTYPE html>
<html lang="es" ng-app="myApp">
<head>
	<title>LogIn</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="vistas/css/main.css">
	<link rel="stylesheet" href="vistas/css/sweetalert2.css">
    <script src="vistas/js/angular.min.js"></script>
    <script src="vistas/js/componentes/ngSweetAlert/SweetAlert.js"></script>
	<script src="vistas/js/componentes/ngSweetAlert/SweetAlert.min.js"></script>
	 <script src="vistas/js/app/app.js"></script>
	  <script src="vistas/js/app/login.js"></script>
		
</head>
<body class="cover " style="background-image: url(vistas/assets/img/administracion.jpg);" ng-controller="myCtrl as log">
	<form class="full-box logInForm" method="post"  name="form" novalidate ng-submit="buscar()">
		<p class="text-center text-muted"><i class="zmdi zmdi-account-circle zmdi-hc-5x"></i></p>
		<p class="text-center text-muted text-uppercase">Inicia sesión con tu cuenta</p>
		<div class="form-group label-floating">
		  <label class="control-label" for="UserEmail">E-mail</label>
		  <input class="form-control" id="UserEmail" type="email" name="email" ng-model="log.user.nombre" minlength="1"  required>
		  <p class="help-block">Escribe tú E-mail</p>
		</div>
		<div class="form-group label-floating">
		  <label class="control-label" for="UserPass">Contraseña</label>
		  <input class="form-control" name="UserPass" id="UserPass" type="password" ng-model="log.user.clave" minlength="1" maxlength="8" required>
		  <p class="help-block">Escribe tú contraseña</p>
		</div>
		<div class="form-group text-center">
			<input type="submit" name="enviar" class="btn btn-raised btn-success" ng-disabled="form.$dirty&& form.$invalid">
		</div>
	</form>
	<!--====== Scripts -->
{{5+5}}
	<script src="vistas/js/jquery-3.1.1.min.js"></script>
	<script src="vistas/js/bootstrap.min.js"></script>
	<script src="vistas/js/material.min.js"></script>
	<script src="vistas/js/ripples.min.js"></script>
	<script src="vistas/js/sweetalert2.min.js"></script>
	<script src="vistas/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="vistas/js/main.js"></script>
	<script>
		$.material.init();
	</script>
</body>
</html>




  
    
        
  
  
      
        
      
        
    
  