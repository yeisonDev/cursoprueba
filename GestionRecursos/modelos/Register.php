<?php

class conection1 extends PDO
{
    private $_host = 'localhost';
    private $_basename = 'codi_prd';
    private $_usuario = 'root';
    private $_contrasena = '';
    private $opciones = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );

    public function __construct()
    {
        try {

            parent::__construct("mysql:host=" . $this->_host . ";dbname=" . $this->_basename, $this->_usuario,
                $this->_contrasena, $this->opciones);
            // set the PDO error mode to exception

        } catch (PDOException $e) {
            echo 'Ha surgido un error y no se puede conectar a la base de datos. Detalle: ' . $e->getMessage();
            exit;
        }
    }
}

class Register
{
    private $_DB;

    public function __construct()
    {
        $this->_DB = new conection1();
    }

    public function newRegister($datos)
    {
        $state = '';
        $this->_DB->beginTransaction();

        $sNom = !(empty($datos->secondName)) ?? "";
        $sApe = !(empty($datos->lastNameTwo)) ?? "";
       // $fVence = !(empty($datos->fVence)) ? $datos->fVence : null;
        $fijo = !(empty($datos->phone)) ?? '';
       // $segmento = !(empty($datos->segmento)) ? $datos->segmento : 0;

        if ($datos->typeId == 1) {
            //Si tipo documento es cédula de ciudadania entonces la fecha de vencimiento de extranjeria debe ser nula
            $fVence = null;
        }

//        if ($afiliado) {
//            $parte = explode('?', $afiliado);
//            $afiliado = $parte[0];
//        }

        $valid = $this->_DB->prepare("SELECT * FROM preregistro WHERE mail = ?");
        $valid->bindParam(1, strtoupper($datos->email), PDO::PARAM_STR);
        $valid->execute();
        if ($valid->rowCount() > 0) {
            $state = 89;
            $m = 'El correo electrónico ya se encuentra registrado';
            jsonReturn($state, $m);
        }

        $validDoc = $this->_DB->prepare("SELECT * FROM preregistro WHERE numDoc = ?");
        $validDoc->bindParam(1, $datos->numId, PDO::PARAM_INT);
        $validDoc->execute();
        if ($validDoc->rowCount() > 0) {
            $state = 88;
            $m = 'El documento de identidad ya se encuentra registrado';
            jsonReturn($state, $m);
        }

/*
        if ($datos->tDoc == 1 and $datos->nacionalidad->id != 1) {
            $m = 'El tipo de documento no corresponde a la nacionalidad seleccionada.';
            $state = 29;
            jsonReturn($state, $m);
        }

        if ($datos->tDoc == 2 and $datos->nacionalidad->id == 1) {
            $m = 'El tipo de documento no corresponde a la nacionalidad seleccionada.';
            $state = 29;
            jsonReturn($state, $m);
        }

        if ($segmento == 0) {
            $ip_sitio = ObtenerIP();
            $valida_ip = $this->valida_ip_punto($ip_sitio);
            if ($valida_ip) {
                $referido = $valida_ip;
            }
        }

        if ($codInscription !== 0) {
            $val_promo = $this->validaPromocional($codInscription);
            if ($val_promo['state'] !== 1) {
                $m = 'El código promocional no se encuentra activo.';
                $state = 24;
                jsonReturn($state, $m);
            }
            $referido = $val_promo['id'];
        }

        if (is_numeric($segmento) AND ($segmento != 0)) {
            $val_segmento = $this->validaSegmento($segmento);
            if (!$val_segmento) {
                $m = 'El código del segmento no se encuentra en nuestra base de datos.';
                $state = 24;
                jsonReturn($state, $m);
            }
            $referido = $segmento;
        } elseif (is_string($segmento)) {
            $val_segmento = $this->validaProveedor($segmento);
            if ($val_segmento['state'] !== 1) {
                $m = 'El nombre del proveedor no se encuentra en nuestra base de datos.';
                $state = 25;
                jsonReturn($state, $m);
            }
            $referido = $val_segmento['id'];
        }

        if (!$referido) {
            $referido = 0;
        }
*/
        //print_r($datos);exit();
        $limites = $this->_DB->query("SELECT limitedep_diario,limitedep_semanal,limitedep_mensual FROM configuracion where config_id = 1");
        $limites->execute();

        $limiteUser = $limites->fetch(PDO::FETCH_OBJ);


        $sentencia = $this->_DB->prepare("INSERT INTO preregistro (tDoc, numDoc, fExp, depart_exp, ciudad_exp,fVence, pNom, sNom, pApe, sApe, sexo, fnace, depart_res, pais, ciudad_res, dir, codP, mail, movil, fijo, clave_reg, t_and_c, created_at, estado, mandante, referido_id, depart_nac, ciudad_nac, segmento, tipo_registro, codigo_groupon,codigo_bono ,limite_dia, limite_semanal, limite_mensual,id_equipo_favorito,cod_promocional, dir_ip,dispositivo,numDocOriginal, afiliador_id) VALUES (:tDoc, :numDoc, :fExp, :depart_exp, :ciud_exp, :fVence, AES_ENCRYPT(:pNom, '" . CLAVE_ENCRYPT . "'), AES_ENCRYPT(:sNom, '" . CLAVE_ENCRYPT . "'), AES_ENCRYPT(:pApe, '" . CLAVE_ENCRYPT . "'), AES_ENCRYPT(:sApe, '" . CLAVE_ENCRYPT . "'), :sexo, :fnace, :depart_res, :nacionalidad, :ciud_res, AES_ENCRYPT(:dir, '" . CLAVE_ENCRYPT . "'), :codP, :mail, AES_ENCRYPT(:movil, '" . CLAVE_ENCRYPT . "'), AES_ENCRYPT(:fijo, '" . CLAVE_ENCRYPT . "'),
            :clave_reg, :t_and_c, NOW(), :estado, :mandante, :referido_id, :depart_nac, :ciud_nac, :segmento, :tipo, :c_groupon, :c_bono,:limDay, :limWeek, :limMont,:idequipo,:codInscription,:ip,:dispositivo, :numDocOriginal, :afiliado)");

        $sentencia->execute(array(
            ':tDoc' => $datos->typeId,
            ':numDoc' => $datos->numId,
            ':fExp' => $datos->dateExp,
           ':depart_exp' => $datos->departEx,
           ':ciud_exp' => $datos->cityEx,
            ':fVence' => $fVence,
            ':pNom' => $datos->firstName,
            ':sNom' => $sNom,
            ':pApe' => $datos->lastName,
            ':sApe' => $sApe,
            ':sexo' => $datos->sex,
            ':fnace' => $datos->dateBirth,
            ':depart_res' => $datos->departRes,
            ':nacionalidad' => $datos->nacionality,
            ':ciud_res' => $datos->cityRes,
            ':dir' => $datos->address,
            //':codP' => $datos->codP->id_codigo_postal,//cityRes
            ':mail' => strtoupper($datos->email),
            ':movil' => $datos->celPhone,
            ':fijo' => $fijo,
            ':clave_reg' => $this->password($datos->pass),
            ':t_and_c' => $datos->habe,
            ':estado' => 'PENDIENTE',
            ':mandante' => MANDANTE,
           // ':referido_id' => $referido,
            ':depart_nac' => $datos->departBirth,
            ':ciud_nac' => $datos->cityBirth,
            //':segmento' => $segmento,
            ':tipo' => 1,
           // ':c_groupon' => $groupon,
           // ':c_bono' => $bono,
            ':limDay' => puntos($limiteUser->limitedep_diario),
            ':limWeek' => puntos($limiteUser->limitedep_semanal),
            ':limMont' => puntos($limiteUser->limitedep_mensual),
           // ':idequipo' => $idEquipo,
           // ':codInscription' => $codInscription,
            ':ip' => ObtenerIP(),
            //':dispositivo' => $datos->dispositivo,
            ':numDocOriginal' => $datos->numId,
           // ':afiliado' => $afiliado
        ));

        if ($sentencia->rowCount() != 1) {
            $state = 85;
            $m = 'Error registrando el usuario ' . var_dump($this->_DB->errorInfo());
            jsonReturn($state, $m);
        }

        if ($state == "") {
            $res = array("state" => 1, "msg" => "El usuario se ha registrado correctamente nuestros operadores validaran la información una vez verificada te enviaremos un correo de bienvenida o de rechazo.");
            $this->_DB->commit();
        } else {
            $this->_DB->rollBack();
            $res = array('state' => 0, "msg" => $state);
        }

        $this->_DB = null;
        echo json_encode($res);
    }
    public function pseudoLlaveAleatoria()
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $rnd = openssl_random_pseudo_bytes(256, $strong);
            if ($strong == true) {
                return $rnd;
            }
        }
        $sha = '';
        $rnd = '';
        for ($i = 0; $i < 256; $i++) {
            $sha = hash('sha256', $sha . mt_rand());
            $char = mt_rand(0, 62);
            $rnd .= chr(hexdec($sha[$char] . $sha[$char + 1]));
        }
        return $rnd;
    }
    public function password($pass)
    {
        $salt = $this->pseudoLlaveAleatoria();
        $hash = '';
        for ($i = 0; $i < 5; $i++) {
            $hash = hash('sha384', $hash . $salt . $pass);
        }
        return base64_encode($salt) . '$' . $hash;
    }

    function validaExclusion($numDoc)
    {
        require_once '../requires/global.php';
        $query = $this->_DB->prepare("SELECT fecha_hora FROM usuario_excluidos WHERE cc = ?");
        $query->bindParam(1, $numDoc, PDO::PARAM_INT);
        $query->execute();
        if ($query->rowCount() > 0) {
            $result = 0;
        } else {
            $result = 1;
        }

        return $result;
    }
    public
    function cedulaExiste($doc)
    {
        include_once '../requires/global.php';

        $q = $this->_DB->prepare("SELECT estado FROM confirmacion_sms_registro WHERE cedula = ? AND estado = ?");
        $q->bindParam(1, $doc, PDO::PARAM_INT);
        $q->bindValue(2, 'R', PDO::PARAM_STR);
        $q->execute();
        if ($q->rowCount() > 0) {
            $result = false;
        } else {
            $result = true;
        }

        if ($result) {

            $stmt = $this->_DB->prepare("SELECT * FROM registro WHERE cedula = ?");
            $stmt->bindParam(1, $doc, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $result = false;
            } else {
                $result = true;
            }

        }

        return $result;
    }
    function validaMovil($movil)
    {

        $consulta = $this->_DB->prepare("SELECT * FROM usuario_celular WHERE celular = SHA2(?,0)");
        $consulta->bindParam(1, $movil, PDO::PARAM_INT);
        $consulta->execute();

        if ($consulta->rowCount() > 0) {

            $result = false;
        } else {

            $result = true;
        }


        return $result;

    }
    public
    function CalculaEdad($fecha)
    {

        list($Y, $m, $d) = explode("-", $fecha);
        return (date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y);
    }

   public function valida_preregistro($data)
    {
        if (strlen($data->firstName) == 0) {
            $m = 'El primer nombre es obligatorio.';
            $s = 41;
            jsonReturn($s, $m);
        }
        if (strlen($data->lastName) == 0) {
            $m = 'El primer apellido es obligatorio.';
            $s = 42;
            jsonReturn($s, $m);
        }
        $sNom='';
        $sApe='';
        if ( !empty($data->secondName)) {
            $sNom = $data->secondName;
        }

        if ( !empty($data->lastNameTwo)) {
            $sApe = $data->lastNameTwo;
        }

        if ( !preg_match("/^[a-zA-Z áéíóúAÉÍÓÚÑñ]+$/", $data->firstName)
            || !preg_match("/^[a-zA-Z áéíóúAÉÍÓÚÑñ]*$/", $sNom)
            || !preg_match("/^[a-zA-Z áéíóúAÉÍÓÚÑñ]+$/", $data->lastName)
            || !preg_match("/^[a-zA-Z áéíóúAÉÍÓÚÑñ]*$/", $sApe)
        ) {
            $m = 'En los campos de nombre y apellido solo pueden ingresarse letras.';
            $s = 43;
            jsonReturn($s, $m);
        }
        if (empty($data->typeId)) {
            $m = 'El tipo de documento no es valido.';
            $s = 44;
            jsonReturn($s, $m);
        }

        if ($data->typeId == 1) {
            $primer_digito = $data->numId[0];

            if ($primer_digito == 0) {
                $m = 'El primer dígito de la cédula no puede ser cero';
                $s = 45;
                jsonReturn($s, $m);
            }
        }

        if (strlen($data->numId) < 3) {
            $m = 'Numero de identificacion invalido.';
            $s = 46;
            jsonReturn($s, $m);
        }

        if (strlen($data->numId) > 15) {
            $m = 'Numero de identificacion invalido.';
            $s = 47;
            jsonReturn($s, $m);
        }

        if ( ! is_numeric($data->numId)) {
            $m = 'Numero de identificacion invalido.';
            $s = 48;
            jsonReturn($s, $m);
        }
      echo 'hola';
        exit();
        $validaExclusion = $this->validaExclusion($data->numId);
        $cedulaExiste = $this->cedulaExiste($data->numId);
        $validaMovil = $this->validaMovil($data->celPhone);
        $edad = $this->CalculaEdad($data->dateBirth);

        if (!$validaExclusion) {
            $m = 'El documento ingresado se encuenta excluido.';
            $s = 49;
            jsonReturn($s, $m);
        }

        if ( ! $cedulaExiste) {
            $m = 'El documento de identidad ya se encuentra registrado.';
            $s = 50;
            jsonReturn($s, $m);
        }

        if ( ! isset($data->dateBirth)) {
            $m = 'La fecha de nacimiento es obligatoria';
            $s = 51;
            jsonReturn($s, $m);
        }

        if ( ! isset($data->dateExp)) {
            $m = 'La fecha de expedición es obligatoria';
            $s = 52;
            jsonReturn($s, $m);
        }

        if (strlen($data->dateExp) != 10) {
            $m
                = 'El formato de la fecha de expedición del documento de identidad no es valido.';
            $s = 53;
            jsonReturn($s, $m);
        }

        $fecha = explode("-", $data->dateExp);
        $anio = strlen($fecha[0]);
        $mes = strlen($fecha[1]);
        $dia = strlen($fecha[2]);

        if ($anio != 4 || $mes != 2 || $dia != 2) {
            $m
                = 'El formato de la fecha de expedición del documento de identidad no es valido.';
            $s = 54;
            jsonReturn($s, $m);
        }

        if ( ! (preg_match('/^([0-9]*)$/',
            $data->cityEx))
        ) {
            $m = 'La ciudad de expedición es invalida.';
            $s = 55;
            jsonReturn($s, $m);
        }

        if ($data->typeId == 2) {
            if (strtotime($data->fVence) < strtotime($data->dateExp)) {
                $m
                    = 'La fecha de vencimiento del documento de extranjera no puede ser anterior a la fecha de expedición';
                $s = 56;
                jsonReturn($s, $m);
            }
        }

        $hoy = getdate();
        $fecha = $hoy['year'] . '-' . $hoy['mon'] . '-' . $hoy['mday'];
        $start_ts = strtotime($fecha);
        $fechaNacimiento = explode("-", $data->dateBirth);

        if ($fechaNacimiento[0] < 1910) {
            $m
                = 'Tienes mas de 100 años enviarnos tu documento para validarlo.';
            $s = 57;
            jsonReturn($s, $m);
        }

        if ($edad < 18) {
            $m
                = 'Debe ser mayor de edad para poder registrarse.';
            $s = 58;
            jsonReturn($s, $m);
        }

        if ($start_ts < strtotime($data->dateExp)) {
            $m
                = 'La fecha de expedición del documento no puede ser mayor que la actual.';
            $s = 59;
            jsonReturn($s, $m);
        }

        if (strtotime($data->fnace) > strtotime($data->dateExp)) {
            $m
                = 'La fecha de expedición del documento de identidad no puede ser anterior a la fecha de nacimiento.';
            $s = 60;
            jsonReturn($s, $m);
        }

        $date1 = new DateTime($data->dateBirth);
        $date2 = new DateTime($data->dateExp);
        $diff = $date1->diff($date2);

        if ($diff->y < 18) {
            $m
                = 'Deben de haber pasado mínimo 18 años entre la fecha de expedición y la fecha de nacimiento';
            $s = 61;
            jsonReturn($s, $m);
        }

        if (empty($data->address)) {
            $m = 'El campo dirección domicilio es obligatorio.';
            $s = 62;
            jsonReturn($s, $m);
        }
        if ( ! preg_match("/^[-.@_A-z0-9]*$/", $data->email)) {
            $m = 'El formato del correo no es correcto.';
            $s = 81;
            jsonReturn($s, $m);
        }
        if ($data->email !== $data->emailConfirm) {
            $m = 'Los campos de correo no coinciden.';
            $s = 63;
            jsonReturn($s, $m);
        }

        if (strlen($data->pass) < 6) {
            $m = 'La contraseña debe tener al menos 6 caracteres.';
            $s = 65;
            jsonReturn($s, $m);
        }
        if ($data->pass !== $data->passConfirm) {
            $m = 'La contraseña no coincide.';
            $s = 66;
            jsonReturn($s, $m);
        }

        if ( ! (isset($data->celPhone))) {
            $m = 'El teléfono móvil es obligatorio.';
            $s = 67;
            jsonReturn($s, $m);
        }

        if (strlen($data->celPhone) != 10) {
            $m
                = 'El teléfono móvil debe de tener solamente 10 dígitos.';
            $s = 68;
            jsonReturn($s, $m);
        }

        $primer_digito = $data->celPhone[0];

        if ($primer_digito != 3) {
            $m = 'El teléfono móvil debe de empezar por 3.';
            $s = 69;
            jsonReturn($s, $m);
        }

        if ( ! is_numeric($data->celPhone)) {
            $m = 'El teléfono móvil tiene campos invadidos.';
            $s = 70;
            jsonReturn($s, $m);

        }

        if ( !$validaMovil) {
            $m
                = 'El teléfono móvil se encuentra registrado';
            $state = 71;
            jsonReturn($state, $m);
        }

        if (!$data->habe) {
            $m
                = 'Debes aceptar los términos y condiciones, habeas data para poder registrarse.';
            $s = 55;
            jsonReturn($s, $m);
        }
        return true;
    }
}

