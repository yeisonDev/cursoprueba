<?php

class Conexion extends PDO
{
    private $_host = 'localhost';
    private $_basename = 'gestionrecursos';
    private $_usuario  = 'root';
    private $_contrasena  = '';
    private $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    public function __construct()
    {
        try {

           parent::__construct("mysql:host=".$this->_host.";dbname=".$this->_basename,$this->_usuario,$this->_contrasena,$this->opciones);
    // set the PDO error mode to exception
        
        } catch (PDOException $e) {
            echo 'Ha surgido un error y no se puede conectar a la base de datos. Detalle: ' . $e->getMessage();
            exit;
        }
    }

}

