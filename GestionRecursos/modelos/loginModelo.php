<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once "../core/configApp.php";
require_once "mainModel.php";
require_once 'conexion.php';


abstract class LoginModeloBd
{
    private $_DB;

    public function __construct()
    {
        $this->_DB = new conexion();
    }

    protected function consultar($correo)
    {
        $correoBd = $correo;

        $sql = "SELECT * 
                 FROM usuario u 
                 INNER JOIN usuario_perfil p 
                 ON u.usuario_id=p.usuario_id 
                 WHERE correo= ? ";

        $consulta = $this->_DB->prepare($sql);

        $consulta->bindParam(1, $correoBd, PDO::PARAM_STR);

        $consulta->execute();

        return $consulta;

    }

}

class LoginModelo extends LoginModeloBd
{
    public function validarLogin($data)
    {
        $user = "";
        $clave = " ";

        if (empty($data->nombre) or empty($data->clave)) {
            jsonReturn(0, 'Llene los campos obligatorios');

        } else {
            $user = limpiar_cadena($data->nombre);
            $clave = limpiar_cadena($data->clave);
        }

        $consulta = $this->consultar($user);


        if ($consulta->rowCount() != 1) {

            jsonReturn(0, 'Usuario o contraseña errada');
        } else {
            $row = $consulta->fetch(PDO::FETCH_OBJ);
            $clave = md5($clave);

            if ($clave == $row->clave) {
                if (session_status() == PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION["logueado"] = true;
                $_SESSION['enLinea'] = date("H:i:s");
                $_SESSION["perfil"] = $row->perfil_id;
                $_SESSION["usuario"] = $row->usuario_id;
                $_SESSION["nombre"] = $row->nombre;
                $_SESSION['time'] = date("Y-m-d H:i:s");
                $_SESSION['foto'] = $row->foto;
                //       $_SESSION['CuentaEstado']= $CuentaEstado;
                // $_SESSION['CuentaGenero']= $CuentaGenero;

                jsonReturn(1, 'Usuario correcto');
            } else {
                jsonReturn(2, 'Usuario o contraseña errada');
            }
        }
    }

    public function logout()
    {
        session_start();
        session_destroy();
        header("location: ../index.php");
    }
}
