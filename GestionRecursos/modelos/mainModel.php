<?php

require_once "../core/configApp.php";
require_once "../modelos/conexion.php";

function limpiar_cadena($cadena)  {
   $cadena=trim($cadena);
   $cadena=stripcslashes($cadena);
   $cadena=str_ireplace("<script>","", $cadena);
   $cadena=str_ireplace("<script src","", $cadena);
   $cadena=str_ireplace("<script type=","", $cadena);
   $cadena=str_ireplace('SELECT * FROM',"", $cadena);
   $cadena=str_ireplace('DELETE * FROM',"", $cadena);
   $cadena=str_ireplace('INSERT INTO',"", $cadena);
   $cadena=str_ireplace('--',"", $cadena);
   $cadena=str_ireplace('[',"", $cadena);
   $cadena=str_ireplace('^',"", $cadena);
   $cadena=str_ireplace('==',"", $cadena);
   return $cadena;
  }


function onlyJsonReturn($msg)
{
    echo json_encode($msg);

    exit();
}

function jsonReturn($stado,$msg){
 echo json_encode(array('stado' => $stado  , 'msg'=> $msg));
exit();
}



function ejecutar_consulta_simple($consulta){

  $sql=$conn->prepare("SELECT * FROM usuario WHERE correo = ?");
  $sql->bindParam(1,$consulta, PDO::PARAM_STR);
  $sql->execute();
  
  if($sql->rowCount()!= 0){
    $respuesta= 1;

  }else  {
    $respuesta = $sql->fetchall(PDO::FETCH_ASSOC);
    
  }
  return  $respuesta;  
  }

 function ejecutar_consulta_simpleid($consulta){
   $conn = new PDO("mysql:host=".SERVER.";dbname=".DB, USER,PASS);
    // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $sql=$conn->prepare("SELECT * FROM cuenta WHERE id = :id");
  $sql->bindParam(":id",$consulta);
  $sql->execute();
    // output data of each row
 $respuesta = $sql->fetch();
 
   
    return  $respuesta;
  
  }



 function AgregarFoto($datos,$Usuario){
$conn = new PDO("mysql:host=".SERVER.";dbname=".DB, USER,PASS);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql=$conn->prepare("UPDATE cuenta set CuentaFoto= :foto WHERE CuentaEmail= :email");
$sql->bindParam(":foto", $datos);
$sql->bindParam(":email", $Usuario);
$sql->execute();
}




function bitacora($datos)
{

}

 function encryption($string)
  {
  	$output=FALSE;
  	$key=hash('sha256', SECRET_KEY);
  	$iv=substr(hash('sha256', SECRET_IV), 0,16);
  	$output=openssl_encrypt($string, METHOD,$key,0,$iv );
  	$output=base64_encode($output);
  	return $output;
  }

 function descryption($string)
  {
  	$key=hash('sha256', SECRET_KEY);
  	$iv=substr(hash('sha256', SECRET_IV), 0,16);
  	$output=openssl_decrypt(base64_decode($string), METHOD, $key,0,	$iv);
  	return $output;
  }

function generar_codigos_aleatorios($letra,$longitud,$num)
   //ESTA FUNCION CREARA NUMEROS ALEATORIOS
  {
  	for ($i=1; $i <$longitud ; $i++) { 
  //rand genera un num aleatorio 
  	$numero=rand(0,9);
  	$letra.=$numero;
  	}
  	return $letra.$num;
  }


