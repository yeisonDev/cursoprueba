<?php
error_reporting(E_ALL);

require_once "../core/configApp.php";
require_once "mainModel.php";
require_once 'conexion.php';
session_start();
class PerfilModel
{

	 private $_DB;
	 private $_usuario= 0;
	function __construct()
	{
		$this->_DB= new conexion();
		$this->_usuario= $_SESSION["usuario"];
	}

// esta funcion tre los datos principales del usuario y los carga a la vista
public function traerDatos(){
$msj='';
$state=0;
$sql= "SELECT nombre, apellido, correo FROM usuario WHERE usuario_id= ?";

$result= $this->_DB->prepare($sql);
$result->bindParam(1,$this->_usuario, PDO::PARAM_INT);
$result->execute();

$respuesta = new stdClass();
if($result->rowCount() > 0){
	   $respuesta->data = $result->fetchAll(PDO::FETCH_ASSOC);
        $this->_DB = null;
        echo json_encode($respuesta);
        return;
	}
  }

  public function actualizarDatos($datosP, $datosC){

      $nombre = $datosP->nombre;
      $apellido = $datosP->apellido;
      $correo = $datosP->correo;
      $usuario = $_SESSION["usuario"];

      $sq = "UPDATE usuario SET nombre = '$nombre', apellido='$apellido', correo= '$correo' WHERE usuario_id=" . $usuario;
      $asql3 = $this->_DB->prepare($sq);
      #$asql3->bindParam(1,$nombre,PDO::PARAM_STR);
      #$asql3->bindParam(2,$apellido,PDO::PARAM_STR);
      #$asql3->bindParam(3,$correo,PDO::PARAM_STR);
      #$asql3->bindParam(4,  $usuario,PDO::PARAM_INT);

      $asql3->execute();


      if ($asql3->rowCount() != 1) {
          $msj = "No se identificaron cambios en la actualizacion";
          $state = 0;
      } else {
          $msj = "Sus datos fueron Actualizados";
          $state = 1;
      }

	   if(!empty($datosC->passA)) {
            $sql = $this->_DB->prepare("SELECT clave FROM usuario WHERE usuario_id=".$usuario);

            $sql->execute();
            $contraBD = $sql->fetchObject();

            if ($sql->rowCount() != 1) {
                $msj = "Error usuario no encontrado";
                $state = 0;
                jsonReturn( $state, $msj);
            } else {
                $contraActual = md5($datosC->passA);
                $contraNueva=md5($datosC->passN);

                if ($contraBD->clave == $contraActual) {
                    $asql2=$this->_DB->prepare("UPDATE usuario SET clave= '$contraNueva' WHERE usuario_id=". $usuario);
                    $asql2->execute();

                    if($asql2->rowCount()!= 1){
                        $msj = "Error! no se actualizo la contraseña";
                        $state = 0;
                    }else{
                        $msj = "Datos actualizados con éxito";
                        $state = 1;

                    }


                }else{
                    $msj = "Contraseña actual errada";
                    $state = 0;
                }

            }
        }


      jsonReturn( $state, $msj);
  }

  public function cambiarFoto(){

if (isset($_FILES))
{

    $file = $_FILES["file"];
    $name = $file["name"];
    $type = $file["type"];
    $tmp_n = $file["tmp_name"];
    $size = $file["size"];
    $folder = "../vistas/assets/img/";
    $msj="";
    $state=0;

    if ($type != 'image/jpg' && $type != 'image/jpeg' && $type != 'image/png' && $type != 'image/gif')
    {
    $msj="Error, el archivo no es una imagen";
    $state=0;

    }
    else if ($size > 1024*1024)
    {
       $msj="Error, el tamaño máximo permitido es un 1MB";
       $state=0;
    }
    else
    {
        $src = $folder.$name;
       if(move_uploaded_file($tmp_n, $src)){

      $sql= $this->_DB->prepare("SELECT * FROM usuario WHERE usuario_id = ?");
      $sql->bindParam(1,$_SESSION["usuario"], PDO::PARAM_STR);
       $sql->execute();

       $respuesta = $sql->fetchAll(PDO::FETCH_ASSOC);

$correo=$respuesta[0]['correo'];

       $sql1=$this->_DB->prepare("UPDATE usuario set foto= '$name' WHERE correo='$correo'");
      $sql1->execute();

      if($sql1->rowCount()>0){
      	$msj="se actualizo foto con exito";

          $sql4= $this->_DB->prepare("SELECT foto FROM usuario WHERE usuario_id = ?");
          $sql4->bindParam(1,$_SESSION["usuario"], PDO::PARAM_STR);
          $sql4->execute();

          $foto=$sql4->fetchAll(PDO::FETCH_ASSOC);

          if($sql4->rowCount()>0){
              $_SESSION["foto"]=$foto[0]['foto'];
          }

      }
    }
  }
}

 echo json_encode($msj);

}

}