<?php
error_reporting( 0 );
session_start();
require 'conexion.php';
require_once "../core/configApp.php";
require_once "mainModel.php";
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class usuario
{
    private  $_DB;
    private $_usuario= 0;
    function __construct()
    {
        $this->_DB= new conexion();
        $this->_usuario= $_SESSION["usuario"];
    }

public function arl (){
        $sqlArl= $this->_DB->prepare('SELECT * FROM arl');
        $sqlArl->execute();
        $result=new stdClass();
        $result->data = $sqlArl->fetchAll( PDO::FETCH_ASSOC );
        $this->_DB = null;
        echo json_encode( $result );
        exit();
    }
    public function cargo (){
        $sqlArl= $this->_DB->prepare('SELECT * FROM cargo');
        $sqlArl->execute();
        $result=new stdClass();
        $result->data = $sqlArl->fetchAll( PDO::FETCH_ASSOC );
        $this->_DB = null;
        echo json_encode( $result );
        exit();
    }
    public function eps (){
        $sqlArl= $this->_DB->prepare('SELECT * FROM eps');
        $sqlArl->execute();
        $result=new stdClass();
        $result->data = $sqlArl->fetchAll( PDO::FETCH_ASSOC );
        $this->_DB = null;
        echo json_encode( $result );
        exit();
    }
    public function perfil (){
        $sqlArl= $this->_DB->prepare('SELECT perfil_id, descripcion FROM perfil');
        $sqlArl->execute();
        $result=new stdClass();
        $result->data = $sqlArl->fetchAll( PDO::FETCH_ASSOC );
        $this->_DB = null;
        echo json_encode( $result );
        exit();
    }

    public function crearUsuario ($data) {
        $fe1 = explode( 'T', $data->ingreso );
        $fe2 = explode( 'T',$data->nacimiento);
        $nacimiento = $fe2[0];
        $ingreso = $fe1[0];
        $nombre = $data->nombre;
        $apellido = $data->apellido;
        $arl = $data->arl;
        $cargo = $data->cargo;
        $cel = $data->cel;
        $correo = $data->correo;
        $dir = $data->dir;
        $eps = $data->eps;
        $estado = $data->estado;
        $pass = md5($data->pass);
        $perfil = $data->perfil;
        $salario = $data->salario;
        $tel = $data->tel;


        $this->_DB->beginTransaction();

        $sql = $this->_DB->prepare( "INSERT     
                                                INTO
                                                    usuario
                                                    (nombre, apellido, clave, correo, foto, telefono,movil,direccion,fecha_nacimiento,salario_basico,fecha_ingreso, fecha_salida, activo, id_cargo, id_eps, id_arl)     
                                                VALUES
                                                    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $sql->bindParam(1,$nombre,PDO::PARAM_STR);
        $sql->bindParam(2,$apellido,PDO::PARAM_STR);
        $sql->bindParam(3,$pass,PDO::PARAM_STR);
        $sql->bindParam(4,$correo,PDO::PARAM_STR);
        $sql->bindValue(5,'avatar.jpg',PDO::PARAM_STR);
        $sql->bindParam(6,$tel,PDO::PARAM_STR);
        $sql->bindParam(7,$cel,PDO::PARAM_STR);
        $sql->bindParam(8,$dir,PDO::PARAM_STR);
        $sql->bindParam(9,$nacimiento,PDO::PARAM_STR);
        $sql->bindParam(10,$salario,PDO::PARAM_INT);
        $sql->bindParam(11,$ingreso,PDO::PARAM_STR);
        $sql->bindValue(12,0,PDO::PARAM_STR);
        $sql->bindParam(13,$estado,PDO::PARAM_INT);
        $sql->bindParam(14,$cargo,PDO::PARAM_INT);
        $sql->bindParam(15,$eps,PDO::PARAM_INT);
        $sql->bindParam(16,$arl,PDO::PARAM_INT);

        if (!$sql->execute()) {

            $this->_DB->rollBack();
            $state = 0;
            $msg = 'Ha ocurrido un error en el 1';
            jsonReturn( $state, $msg );
        }
        if ($sql->rowCount()!== 1) {
            $this->_DB->rollBack();
            $state = 0;
            $msg = 'Ha ocurrido un error en el registro 2';
            jsonReturn( $state, $msg );
        }
        $sql2 = $this->_DB->prepare( 'INSERT INTO usuario_perfil (perfil_id) VALUES (?)');
        $sql2->bindParam( 1, $perfil, PDO::PARAM_STR );
        if (!$sql2->execute()) {
            $state = 0;
            $this->_DB->rollBack();
            $msg = 'Ha ocurrido un error en el registro 3';
            jsonReturn( $state, $msg );
        }
        if ($sql2->rowCount()!== 1 ) {
            $state = 0;
            $this->_DB->rollBack();
            $msg = 'Ha ocurrido un error en el registro 4';
            jsonReturn( $state, $msg );
        }
        $this->_DB->commit();
        $this->_DB = null;
        $state = 1;
        $msg = 'Usuario registrado con exito';
        jsonReturn( $state, $msg );
    }

    public function tablero () {
        $sql= $this->_DB->prepare( "SELECT perfil_id, count(perfil_id) as cantidad FROM  usuario_perfil where perfil_id = 'ADMIN' group by perfil_id" );
        $sql->execute();
        $admin = $sql->fetch( PDO::FETCH_OBJ );
        $sql1= $this->_DB->prepare( "SELECT perfil_id, count(perfil_id) as cantidad FROM  usuario_perfil where perfil_id = 'USU' group by perfil_id" );
        $sql1->execute();
        $usuario = $sql1->fetch( PDO::FETCH_OBJ );
        $sql2= $this->_DB->prepare( "SELECT perfil_id, count(perfil_id) as cantidad FROM  usuario_perfil where perfil_id = 'CONTA' group by perfil_id" );
        $sql2->execute();
        $contabilidad = $sql2->fetch( PDO::FETCH_OBJ );
        $sql3= $this->_DB->prepare( "SELECT perfil_id, count(perfil_id) as cantidad FROM  usuario_perfil where perfil_id = 'TECNO' group by perfil_id" );
        $sql3->execute();
        $tecnologia = $sql3->fetch( PDO::FETCH_OBJ );

       $resul= new stdClass();
       $resul->admin = $admin->cantidad;
       $resul->usu = $usuario->cantidad;
       $resul->conta = $contabilidad->cantidad;
        $resul->tecno = $tecnologia->cantidad;

        echo json_encode( $resul );
        exit();
    }

    public function listarUsuarios ()
    {
        $sql = $this->_DB->prepare( 'SELECT 
                                                u.usuario_id,
                                                u.nombre,
                                                u.apellido,
                                                u.direccion,
                                                u.correo,
                                                u.movil,
                                                p.descripcion,
                                                c.cargo,
                                                e.eps,
                                                a.arl
                                            FROM
                                                usuario u
                                                    LEFT JOIN
                                                usuario_perfil up ON u.usuario_id = up.usuario_id
                                                    INNER JOIN
                                                perfil p ON up.perfil_id = p.perfil_id
                                                    INNER JOIN
                                                cargo c ON u.id_cargo = c.idcargo
                                                    INNER JOIN
                                                eps e ON u.id_eps = e.ideps
                                                    INNER JOIN
                                                arl a ON u.id_arl = a.idarl' );

        $sql->execute();

        $result = new stdClass();
        $result->data = $sql->fetchAll( PDO::FETCH_ASSOC );
        $this->_DB = null;
        echo json_encode( $result );
        exit();
    }

    public function datosUsuario ($data) {
        $usuId = $data;
        $sql = $this->_DB->prepare( 'SELECT 
                                                u.nombre,
                                                u.apellido,
                                                u.direccion,
                                                u.correo,
                                                u.movil,
                                                up.perfil_id,
                                                u.activo,
                                                u.salario_basico,
                                                u.telefono,
                                                u.id_cargo,
                                                u.id_arl,
                                                u.id_eps
                                            FROM
                                                gestionrecursos.usuario u
                                                    inner JOIN
                                                usuario_perfil up ON u.usuario_id = up.usuario_id where u.usuario_id= ?');
        $sql->bindParam( 1, $usuId, PDO::PARAM_INT );
        $sql->execute();

        $result = new stdClass();
        $result->data = $sql->fetchAll( PDO::FETCH_ASSOC );
        $this->_DB = null;
        echo json_encode( $result );
        exit();
    }

    public function actualizarUsuario ($data)
    {
        $usuId = $data->id;
        $nombre = $data->nombre;
        $apellido = $data->apellido;
        $arl = $data->arl;
        $cargo = $data->cargo;
        $cel = $data->cel;
        $tel = $data->tel;
        $correo = $data->correo;
        $dir = $data->dir;
        $eps = $data->eps;
        $estado = $data->estado;
        $perfil = $data->perfil;
        $salario = $data->salario;
        $tel = $data->tel;

        $sql = $this->_DB->prepare( 'UPDATE usuario 
                                           inner join usuario_perfil 
                                             ON usuario.usuario_id = usuario_perfil.usuario_id
                                                SET 
                                                    nombre = ?,
                                                    apellido = ?,
                                                    correo = ?,
                                                    telefono = ?,
                                                    movil = ?,
                                                    direccion = ?,
                                                    salario_basico = ?,
                                                    activo = ?,
                                                    id_cargo = ?,
                                                    id_eps = ?,
                                                    id_arl = ?,
                                                    perfil_id=?
                                                WHERE
                                                    usuario.usuario_id = ?' );

        $sql->bindParam( 1, $nombre, PDO::PARAM_STR );
        $sql->bindParam( 2, $apellido, PDO::PARAM_STR );
        $sql->bindParam( 3, $correo, PDO::PARAM_STR );
        $sql->bindParam( 4, $tel, PDO::PARAM_STR );
        $sql->bindParam( 5, $cel, PDO::PARAM_STR );
        $sql->bindParam( 6, $dir, PDO::PARAM_STR );
        $sql->bindParam( 7, $salario, PDO::PARAM_INT );
        $sql->bindParam( 8, $estado, PDO::PARAM_INT );
        $sql->bindParam( 9, $cargo, PDO::PARAM_INT );
        $sql->bindParam( 10, $eps, PDO::PARAM_INT );
        $sql->bindParam( 11, $arl, PDO::PARAM_INT );
        $sql->bindParam( 12, $perfil, PDO::PARAM_STR );
        $sql->bindParam( 13, $usuId, PDO::PARAM_INT );

        $sql->execute();

        if ($sql->rowCount() === 0) {
            $state = 0;
            $msg = "No se ha realizado la actualizacion.";
            jsonReturn( $state, $msg );
        } elseif ($sql->rowCount() > 0) {
            $state = 1;
            $msg = "Usuario Actualizado Correctamente.";
            jsonReturn( $state, $msg );
        }
    }
}
