<?php

function ObtenerIP()
{
    foreach (['HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR'] as $key) {

        if (array_key_exists($key, $_SERVER)) {
            foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip) {

                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                    return $ip;
                }
            }
        }
    }

    return '?';
}

/*$dip = ObtenerIP();
$json = file_get_contents("https://ipinfo.io/" . $dip);
$details = json_decode($json, true);
if (array_key_exists("ip", $details)) $ip .= $details["ip"];
if (array_key_exists("city", $details)) $city .= $details["city"];
if (array_key_exists("region", $details)) $region .= $details["region"];
if (array_key_exists("country", $details)) $country .= $details["country"];
if (array_key_exists("loc", $details)) $loc .= $details["loc"];
if (array_key_exists("org", $details)) $org .= $details["org"];*/

/*echo "Direcci&#243;n IP: " . $ip . "<br>";
echo "Ciudad: " . $city . "<br>";
echo "Regi&#243;n: " . $region . "<br>";
echo "Pa&#237;s: " . $country . "<br>";
echo "Localizaci&#243;n: " . $loc . "<br>";
echo "Proveedor de internet: " . $org . "<br>";*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>formulario pago - api</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">
<div class="row">
        <div class="col-sm-6">
            <div class="page-header"><h1>Pago Api</h1></div>
            <!-- FORM -->
            <form name="userForm" id= "form">
                <div class="form-group">
                    <label>Documento</label>
                    <input type="number" name="numero_documento" class="form-control" id="documento">

                </div>
                <div class="form-group">
                    <label>Cedula</label>
                    <input type="number" name="cedula" class="form-control" id="cedula">

                </div>
                <div class="form-group">
                    <label>Clave</label>
                    <input type="number" name="clave_documento" class="form-control" id="clave">

                </div>
                <div class="form-group">
                    <label>id_punto</label>
                    <input type="number" name="id_punto" class="form-control" id="id_punto">

                </div>
                <div class="form-group">
                    <label>id_transaccion</label>
                    <input type="number" name="id_transaccion" class="form-control" id="id_transaccion">

                </div>
                <div class="form-group">
                    <label>token_seguridad</label>
                    <input type="text" name="token_seguridad" class="form-control" id="token_seguridad">

                </div>
                <input type="button" class="btn btn-primary" onclick="submitForm()" value="enviar">
            </form>
        </div>

    <div class="col-sm-6">
        <div class="page-header"><h1>recarga</h1></div>
        <!-- FORM -->
        <form name="userFormR" id= "formR">
            <div class="form-group">
                <label>id_punto</label>
                <input type="number" name="id_punto" class="form-control" id="id_punto">

            </div>
            <div class="form-group">
                <label>id_transaccion</label>
                <input type="number" name="id_transaccion" class="form-control" id="id_transaccion">

            </div>
            <div class="form-group">
                <label>valor</label>
                <input type="number" name="valor" class="form-control" id="valor">

            </div>
            <div class="form-group">
                <label>id_subpunto</label>
                <input type="number" name="id_subpunto" class="form-control" id="id_subpunto">

            </div>
            <div class="form-group">
                <label>cedula</label>
                <input type="number" name="cedula" class="form-control" id="cedula">

            </div>
            <div class="form-group">
                <label>token_seguridad</label>
                <input type="text" name="token_seguridad" class="form-control" id="token_seguridad">

            </div>
            <input type="button" class="btn btn-primary" onclick="submitFormR()" value="enviar">
        </form>
    </div>
    </div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
  // var postApp = angular.module('postApp', []);
  // postApp.controller('postController', function ($scope, $http) {
  //   $scope.user = {};
  //   $scope.submitForm = function () {
  //
  //     /*$http.post('https://devaliados.wplay.co/actions/pagos/pago_api.php', {
  //       data: $scope.user,
  //       headers: {'Content-Type': 'Content-Type: application/json'}
  //     }).then(
  //       function (response) {
  //         console.log('kilo ', response);
  //       }, function (error) {
  //         console.log(error);
  //       });*/
  //
  //     $http({
  //       method: 'POST',
  //       url: 'https://devaliados.wplay.co/actions/pagos/pago_api.php',
  //       data: $scope.user,
  //       headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  //     })
  //       .success(function (data) {
  //         console.log('W ', data);
  //         // if (data.errors) {
  //         //   $scope.errorName = data.errors.name;
  //         //   $scope.errorUserName = data.errors.username;
  //         //   $scope.errorEmail = data.errors.email;
  //         // } else {
  //         //   $scope.message = data.message;
  //         // }
  //       });
  //   };
  // });


  function submitForm() {


      var documento= document.getElementById("documento").value;
      var cedula= document.getElementById("cedula").value;
      var clave= document.getElementById("clave").value;
      var id_punto= document.getElementById("id_punto").value;
      var id_transaccion= document.getElementById("id_transaccion").value;
      var token_seguridad= document.getElementById("token_seguridad").value;

      var data= {
          numero_documento:documento,
          cedula:cedula,
          clave_documento:clave,
          id_punto:id_punto,
          id_transaccion:id_transaccion,
          token_seguridad:token_seguridad
      };



      fetch('https://devaliados.wplay.co/actions/pagos/pago_api.php',
          {method:'post',
              body:JSON.stringify(data)})
          .then(function(response) { return response.json(); })
          .then(function(data) {
              console.log(data.data.code);
              if (data.data.code==11){
                  f();
              }

          });

      function f() {
          var data1= {
              numero_documento:documento,
              cedula:cedula,
              clave_documento:clave,
              id_punto:id_punto,
              id_transaccion:id_transaccion,
              token_seguridad:token_seguridad,
              estadoProceso:"true"
          };

          fetch('https://devaliados.wplay.co/actions/pagos/pago_api.php',
              {method:'post',
                  body:JSON.stringify(data1)})
              .then(function(response) { return response.json(); })
              .then(function(data) {
                  console.log(data.data);
              });
      }

  };

function submitFormR() {
    var formulario = document.getElementById('formR');



    var datos = new FormData(formulario);
    var data= {
        id_punto:datos.get('id_punto'),
        id_transaccion:datos.get('id_transaccion'),
        valor:datos.get('valor'),
        id_subpunto:datos.get('id_subpunto'),
        cedula:datos.get('cedula'),
        token_seguridad:datos.get('token_seguridad')
    };

    console.log(datos.get('id_punto'));

    fetch('https://devaliados.wplay.co/actions/punto-venta/recargas_api.php',
        {method:'post',
            body:JSON.stringify(data)})
        .then(function(response) { return response.json(); })
        .then(function(data) {
           console.log(data.data.code);

           if(data.data.code== 9){
               recargar();

           }
       });

    function recargar() {
        var data2= {
            id_punto:datos.get('id_punto'),
            id_transaccion:datos.get('id_transaccion'),
            valor:datos.get('valor'),
            id_subpunto:datos.get('id_subpunto'),
            cedula:datos.get('cedula'),
            token_seguridad:datos.get('token_seguridad'),
            estadoProceso:"true"
        };

        fetch('https://devaliados.wplay.co/actions/punto-venta/recargas_api.php',
            {method:'post',
                body:JSON.stringify(data2)})
            .then(function(response) { return response.json(); })
            .then(function(data) {
                console.log(data.data);
            });

    }

}

</script>
</body>
</html>

