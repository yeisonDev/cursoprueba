-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-06-2019 a las 16:16:02
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestionrecursos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arl`
--

CREATE TABLE `arl` (
  `idarl` int(11) NOT NULL,
  `arl` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `arl`
--

INSERT INTO `arl` (`idarl`, `arl`) VALUES
(1, 'Sura'),
(2, 'Salud Total');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `idcargo` int(11) NOT NULL,
  `cargo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='esta tabla contendra los cargos de los empleados';

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`idcargo`, `cargo`) VALUES
(1, 'Operario'),
(2, 'Supervisor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE `eps` (
  `ideps` int(11) NOT NULL,
  `eps` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Esta tabla contendra la eps de los trabajadores ';

--
-- Volcado de datos para la tabla `eps`
--

INSERT INTO `eps` (`ideps`, `eps`) VALUES
(1, 'Sura '),
(2, 'Salud Total'),
(3, 'Sabia Salud');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `menu_id` bigint(20) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `pagina` varchar(50) NOT NULL,
  `orden` int(20) NOT NULL,
  `icono` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`menu_id`, `descripcion`, `pagina`, `orden`, `icono`) VALUES
(1, 'Administrar', 'usuario.php', 1, 'zmdi zmdi-account-add zmdi-hc-fw'),
(2, 'Usuarios', '', 2, 'zmdi zmdi-book zmdi-hc-fw');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `perfil_id` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`id`, `descripcion`, `perfil_id`) VALUES
(1, 'Administrador', 'ADMIN'),
(2, 'Usuario', 'USU'),
(3, 'Contabilidad', 'CONTA'),
(4, 'Tecnologia', 'TECNO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_submenu`
--

CREATE TABLE `perfil_submenu` (
  `id` bigint(20) NOT NULL,
  `perfil_id` varchar(50) NOT NULL COMMENT 'Se agregan los direntes perfiles y le asignamos que pueden ver',
  `submenu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfil_submenu`
--

INSERT INTO `perfil_submenu` (`id`, `perfil_id`, `submenu_id`) VALUES
(1, 'ADMIN', 1),
(2, 'ADMIN', 2),
(3, 'ADMIN', 3),
(6, 'USU', 2),
(7, 'USU', 3),
(8, 'CONTA', 2),
(9, 'CONTA', 3),
(10, 'TECNO', 1),
(11, 'TECNO', 2),
(12, 'TECNO', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `submenu`
--

CREATE TABLE `submenu` (
  `submenu_id` bigint(20) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `pagina` varchar(100) NOT NULL,
  `menu_id` int(100) NOT NULL,
  `folder` varchar(30) NOT NULL,
  `iconos_sub` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `submenu`
--

INSERT INTO `submenu` (`submenu_id`, `descripcion`, `pagina`, `menu_id`, `folder`, `iconos_sub`) VALUES
(1, 'Nuevo Registro', 'usuario_admin.php', 1, 'vistas', 'zmdi zmdi-account zmdi-hc-fw'),
(2, 'Generar colilla de pago', '#', 2, 'vistas', 'zmdi zmdi-account zmdi-hc-fw'),
(3, 'Carta laboral', '#', 2, 'vistas', 'zmdi zmdi-account zmdi-hc-fw');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` bigint(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `foto` varchar(30) NOT NULL,
  `telefono` varchar(10) NOT NULL DEFAULT '0',
  `movil` char(11) NOT NULL DEFAULT '0',
  `direccion` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` varchar(20) NOT NULL DEFAULT '0',
  `salario_basico` double NOT NULL,
  `fecha_ingreso` varchar(20) NOT NULL,
  `fecha_salida` varchar(20) NOT NULL,
  `activo` char(1) NOT NULL DEFAULT '1',
  `id_cargo` int(11) DEFAULT NULL,
  `id_eps` int(11) DEFAULT NULL,
  `id_arl` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `nombre`, `apellido`, `clave`, `correo`, `foto`, `telefono`, `movil`, `direccion`, `fecha_nacimiento`, `salario_basico`, `fecha_ingreso`, `fecha_salida`, `activo`, `id_cargo`, `id_eps`, `id_arl`) VALUES
(1, 'Yeison', 'Guzman', '827ccb0eea8a706c4c34a16891f84e7b', 'yeison955@gmail.com', 'yei.PNG', '0', '0', 'calle 79 AA 30 a 32 Apto 117', '0', 0, '', '', '1', 1, 1, 1),
(2, 'Dani', 'vasquez', '827ccb0eea8a706c4c34a16891f84e7b', 'yeuuu@gmail.com', 'blur-background0999.jpg', '2345678', '3012345566', 'calle 79 AA 30 a 32 Apto 117', '2019-06-18', 800000, '2019-06-20', '0', '1', 1, 1, 1),
(3, 'juan', 'perez', '827ccb0eea8a706c4c34a16891f84e7b', 'perez@gmail.com', 'avatar.jpg', '2345678', '3013456677', 'calle 79 AA 30 a 32 Apto 117', '2019-06-11', 1000000, '2019-06-14', '0', '1', 2, 2, 2),
(4, 'carlos', 'gonzales', '827ccb0eea8a706c4c34a16891f84e7b', 'gonzalez@gmail.com', 'avatar.jpg', '3456789', '3004344444', 'calle 79 AA 30 a 32 Apto 117', '2019-06-25', 2000000, '2019-06-28', '0', '1', 2, 3, 1),
(5, 'Dani', 'perez', '4739c5c11d833bb199c16ff95a92b267', 'prueb@gmail.co', 'avatar.jpg', '23445554', '3016894444', 'calle 79 AA 30 a 32 Apto 15', '2019-06-20', 800000, '2019-06-08', '0', '1', 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_perfil`
--

CREATE TABLE `usuario_perfil` (
  `usuario_id` bigint(20) NOT NULL,
  `perfil_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_perfil`
--

INSERT INTO `usuario_perfil` (`usuario_id`, `perfil_id`) VALUES
(1, 'ADMIN'),
(2, 'CONTA'),
(3, 'CONTA'),
(4, 'TECNO'),
(5, 'USU');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `arl`
--
ALTER TABLE `arl`
  ADD PRIMARY KEY (`idarl`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`idcargo`);

--
-- Indices de la tabla `eps`
--
ALTER TABLE `eps`
  ADD PRIMARY KEY (`ideps`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perfil_submenu`
--
ALTER TABLE `perfil_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`submenu_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`);

--
-- Indices de la tabla `usuario_perfil`
--
ALTER TABLE `usuario_perfil`
  ADD PRIMARY KEY (`usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `arl`
--
ALTER TABLE `arl`
  MODIFY `idarl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `idcargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `eps`
--
ALTER TABLE `eps`
  MODIFY `ideps` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `perfil_submenu`
--
ALTER TABLE `perfil_submenu`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `submenu`
--
ALTER TABLE `submenu`
  MODIFY `submenu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario_perfil`
--
ALTER TABLE `usuario_perfil`
  MODIFY `usuario_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
