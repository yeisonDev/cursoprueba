<!DOCTYPE html>
<html lang="es" ng-app="myApp">
<head>
	<title> <?php echo $titulo;?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../vistas/css/main.css">
	<link rel="stylesheet" href="../vistas/css/sweetalert2.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="../vistas/js/angular.min.js"></script>
	<script src="../vistas/js/componentes/ngSweetAlert/SweetAlert.js"></script>
	<script src="../vistas/js/componentes/ngSweetAlert/SweetAlert.min.js"></script>
	<script src="../vistas/js/app/app.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <?php

    /**
     *  Incluye los archivos css que se incluyeron en el array js_files
     */

    if (isset($js_files)) {

        foreach ($js_files as $jsf) {

            echo $jsf;
        }
    }

     require_once '../core/configGeneral.php';
    require_once '../modelos/conexion.php';

   $_DB = new Conexion;
 $sql = $_DB->prepare("SELECT
                            m.menu_id,

                            m.descripcion,

                            m.pagina,
                            
                            m.icono

                        FROM

                            menu m

                        INNER JOIN submenu s ON  m.menu_id = s.menu_id

                        INNER JOIN perfil_submenu p ON s.submenu_id = p.submenu_id

                        WHERE
                            perfil_id = :perfil
                        GROUP BY

                            menu_id ASC");

    $sql->execute(array(':perfil' => $_SESSION["perfil"]));//pendiente para ponerle la session

    ?>

	<script src="../vistas/js/app/directives.js"></script>
</head>
<body>
	<!-- SideBar -->
	<section class="full-box cover dashboard-sideBar">
		<div class="full-box dashboard-sideBar-bg btn-menu-dashboard"></div>
		<div class="full-box dashboard-sideBar-ct">
			<!--SideBar Title -->
			<div class="full-box text-uppercase text-center text-titles dashboard-sideBar-title">
				<?php echo COMPANIA; ?><i class="zmdi zmdi-close btn-menu-dashboard visible-xs"></i>
			</div>
			<!-- SideBar User info -->
			<div class="full-box dashboard-sideBar-UserInfo">
				<figure class="full-box">
                    <?php if(!isset($_SESSION['foto'])){?>
                        <img class="thumb-image " id="imagen" src="../vistas/assets/img/avatar3.png" alt="UserIcon" />
                    <?php } else {;?>
                        <img class="thumb-image " id="imagen" src="../vistas/assets/img/<?php echo $_SESSION['foto'];?>" alt="UserIcon"/>
                    <?php };?>
					<figcaption class="text-center text-titles"><?php echo $_SESSION['nombre'];?></figcaption>
				</figure>
				<ul class="full-box list-unstyled text-center">
					<li>
						<a href="Miperfil.php";>
							<i class="zmdi zmdi-settings"></i>
						</a>
					</li>
					<li>

						<a href="../controladores/logoutControlador.php" class="">
							<i class="zmdi zmdi-power"></i>
						</a>
					</li>
				</ul>
			</div>
			<!-- SideBar Menu -->
			<ul class="list-unstyled full-box dashboard-sideBar-Menu">
				<li>
					<a href="home.php">
						<i class="zmdi zmdi-view-dashboard zmdi-hc-fw"></i> Tablero
					</a>
				</li>

				<?php

				  while ($row = $sql->fetch(PDO::FETCH_OBJ)) {

				 $sub_m = "SELECT s.descripcion, s.pagina, s.menu_id,s.folder,s.iconos_sub FROM submenu s INNER JOIN perfil_submenu p ON s.submenu_id = p.submenu_id WHERE perfil_id = :perfil AND menu_id = :m ORDER BY s.descripcion";

				 $sub_stmt = $_DB->prepare($sub_m);

				 $sub_stmt->bindParam(':m', $row->menu_id, PDO::PARAM_INT);

				 $sub_stmt->bindParam(':perfil',$_SESSION["perfil"], PDO::PARAM_STR);

				 $sub_stmt->execute();

				 ?>

        <li>

            <a href="javascript:void(0)" class="btn-sideBar-SubMenu"><i class="<?php echo $row->icono; ?>"></i><?php echo " ".$row->descripcion; ?><i class="zmdi zmdi-caret-down pull-right"></i></a>

           <?php if ($sub_stmt->rowCount()) { ?>
                <ul class="list-unstyled full-box">
                   <?php
                    while ($sub_row = $sub_stmt->fetch(PDO::FETCH_OBJ)) {
                       ?>
                        <li>
                            <a href="<?php echo $sub_row->pagina; ?>"><i class="<?php echo $sub_row->iconos_sub; ?>"></i><?php echo  $sub_row->descripcion; ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            }
            ?>
        </li>
<?php } ?>
			</ul>
		</div>
	</section>

	<!-- Content page-->
	<section class="full-box dashboard-contentPage">
		<!-- NavBar -->
		<nav class="full-box dashboard-Navbar">
			<ul class="full-box list-unstyled text-right">
				<li class="pull-left">
					<a href="#!" class="btn-menu-dashboard"><i class="zmdi zmdi-more-vert"></i></a>
				</li>
				<li>
					<a href="#!" class="btn-Notifications-area">
						<i class="zmdi zmdi-notifications-none"></i>
						<span class="badge">7</span>
					</a>
				</li>
				<li>
					<a href="#!" class="btn-search">
						<i class="zmdi zmdi-search"></i>
					</a>
				</li>
				<li>
					<a href="#!" class="btn-modal-help">
						<i class="zmdi zmdi-help-outline"></i>
					</a>
				</li>
			</ul>
		</nav>

		<!-- Content page -->



