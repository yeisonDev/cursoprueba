(function(){
'use strict';
angular.module("myApp")
     .directive("equal", function () {
       return {
           require: "ngModel", scope: {equal: "="}, link: function (scope, elm, attrs, ctrl) {
               ctrl.$validators.equal = function (modelValue, viewValue) {
                   return modelValue === scope.equal
               };
               scope.$watch("equal", function (newVal, oldVal) {
                   ctrl.$validate()
               })
           }
       }
   }).directive("numbersOnly", function () {
       return {
           require: "ngModel", link: function (scope, element, attr, ngModelCtrl) {
               function fromUser(text) {
                   if (text) {
                       var transformedInput = text.replace(/[^0-9]/g, "");
                       if (transformedInput !== text) {
                           ngModelCtrl.$setViewValue(transformedInput);
                           ngModelCtrl.$render()
                       }
                       return transformedInput
                   }
                   return undefined
               }
               ngModelCtrl.$parsers.push(fromUser)
           }
       }
   }).directive('ngEnter', function () {
       return function (scope, element, attrs) {
           element.bind("keydown keypress", function (event) {
               if (event.which === 13) {
                   scope.$apply(function () {
                       scope.$eval(attrs.ngEnter);
                   });
                   event.preventDefault();
               }
           });
       };
   }).directive('datePicker', function() {
   return {
       require: 'ngModel',
       link: function(scope, el, attr, ngModel) {
           $(el).datepicker({
               onSelect: function(dateText) {
                   scope.$apply(function() {
                       ngModel.$setViewValue(dateText);
                   });
               }
           });
       }
   };
}).directive("fileInput", function($parse){
return{
    link: function($scope, element, attrs){
    element.on("change", function(event){
    var files = event.target.files;
    $parse(attrs.fileInput).assign($scope, element[0].files);
    $scope.$apply();
   });
  }
}
}).directive('onlyLettersInput',function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^a-zñáéíóúA-ZÑÁÉÍÓÚ]/g, '');
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }

            ngModelCtrl.$parsers.push(fromUser);
        },
    };
}).directive('myDate', ['$timeout', '$filter', function ($timeout, $filter)
{
    return {
        require: 'ngModel',

        link: function ($scope, $element, $attrs, $ctrl)
        {
            var dateFormat = 'yyyy-MM-dd';
            $ctrl.$parsers.push(function (viewValue)
            {
                //convert string input into moment data model
                var pDate = Date.parse(viewValue);
                if (isNaN(pDate) === false) {
                    return new Date(pDate);
                }
                return undefined;

            });
            $ctrl.$formatters.push(function (modelValue)
            {
                var pDate = Date.parse(modelValue);
                if (isNaN(pDate) === false) {
                    return $filter('date')(new Date(pDate), dateFormat);
                }
                return undefined;
            });
            $element.on('blur', function ()
            {
                var pDate = Date.parse($ctrl.$modelValue);
                if (isNaN(pDate) === true) {
                    $ctrl.$setViewValue(null);
                    $ctrl.$render();
                } else {
                    if ($element.val() !== $filter('date')(new Date(pDate), dateFormat)) {
                        $ctrl.$setViewValue($filter('date')(new Date(pDate), dateFormat));
                        $ctrl.$render();
                    }
                }

            });
            $timeout(function ()
            {
                $element.kendoDatePicker({

                    format: dateFormat
                });

            });
        }
    };
}]);
})();
