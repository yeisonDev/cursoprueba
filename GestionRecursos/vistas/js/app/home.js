(function () {
    'use strict';
    angular
        .module('myApp')
         .controller('homeCtrl', homeCtrl);
    homeCtrl.$inject = ['$http'];

    function homeCtrl($http) {
        var vm = this;
        vm.cargaDatos= function () {
            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'tablero'
            }).then(function(response) {
                 var response=response.data;
                vm.usu = response.usu;
                vm.admin = response.admin;
                vm.conta = response.conta;
                vm.tecno = response.tecno;
            });

        };
    }

})();
