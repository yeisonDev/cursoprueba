(function () {
  'use strict'

  angular.module('myApp').
    service('miService', miService).
    controller('myCtrl', myCtrl)
  miService.$inject = ['$http', '$q']
  myCtrl.$inject = ['$scope', 'SweetAlert', 'miService']

  function miService ($http, $q) {
    function send (url, data) {
      return $http.post(url, data).then(complete).catch(failed)

      function complete (response) {
        return $q.when(response.data)
      }

      function failed (response) {
        return $q.reject(response.data)
      }
    }

    return {
      send: send
    }
  }

  function myCtrl ($scope, SweetAlert, miService) {
    var vm = this
    vm.user = {}

    $scope.buscar = function () {
      miService.send(
        '../../../../GestionRecursos/controladores/loginControlador.php', {
          metodo: 'login',
          datos: vm.user

        }).then(function (response) {
        if (response.stado == 1) {
          SweetAlert.swal('Bienvenido',
            'Usuario Correcto!',
            'success'
          )
          window.location.href = '../../../../GestionRecursos/action/home.php'
        }
        else {
          SweetAlert.swal(response.msg,
            'Intente de nuevo',
            'error'
          )
        }

      }).catch(function (data) {
        console.log(data)
      })
    }
  }

})()

