(function(){

'use strict';

angular
 .module("myApp")
   .controller('miperfil', miperfil);
miperfil.$inject=['$http','$scope','SweetAlert'];

 function miperfil($http,$scope,SweetAlert){
var vm =this;
vm.contra={};
vm.usuario={};
vm.files={};

// esta funcion tre los datos principales del usuario y los carga a la vista	
vm.inicio= function(){
$http.post('../../../../GestionRecursos/controladores/perfilControlador.php', {
            metodo: 'traer'  
     }).then(function(response) {
     	vm.usuario=response.data.data[0];
      console.log(response.data.data[0].nombre);
  });
};

vm.actualizarDatos= function(){
    $http.post('../../../../GestionRecursos/controladores/perfilControlador.php', {
        metodo: 'actualizarDatos',
        datos1:  vm.usuario,
        datos2: vm.contra

    }).then(function (response) {
        response = response.data;
        if(response.stado==1){
            SweetAlert.swal(response.msg,
                "Datos actualizados!",
                "success"
            );
        }else{
            SweetAlert.swal(response.msg,
                "Alerta!",
                "error"
            );
        }

    });
};

$scope.cambiarfoto= function(){
    var form_data= new FormData();
    angular.forEach($scope.files,function (file) {
        form_data.append('file',file);
    });
    $http.post('../../../../GestionRecursos/controladores/ejemplo.php',form_data,
        {
            transformRequest: angular.identity,
            header:{'Content-Type': undefined,'process-Data': false}
        }).then(function (response) {
        alert(response);
    });
}

}

})()
