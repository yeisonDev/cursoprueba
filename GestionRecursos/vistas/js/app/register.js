(function() {
  'use strict';
  angular.module('myApp').
      service('myService', myService).
      controller('registerCtrl', registerCtrl);
  myService.$inject = ['$http', '$q'];
  registerCtrl.$inject = ['$scope', 'myService'];

  function myService($http, $q) {

    function myPost(url, data) {
      return $http.post(url, data).then(complete).catch(failed);

      function complete(response) {
        return $q.when(response.data);
      }

      function failed(response) {
        return $q.reject(response.data);
      }
    }

    return {
      myPost: myPost,
    };
  }

  function registerCtrl($scope, myService) {
    let vm = this;
    vm.register= {};
    let data= {
      method: 'newRegister',
      data: vm.register
    };

    vm.newRegist= function() {
      myService.myPost('../../../../GestionRecursos/controladores/registerControlador.php',
          data).then((response)=> {
      });
    }

  }

})();
