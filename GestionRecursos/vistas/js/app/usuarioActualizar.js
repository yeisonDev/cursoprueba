(function () {
    'use strict';
   angular.module('myApp')
       .controller('usuarioActuCtrl',usuarioActuCtrl);
    usuarioActuCtrl.$inject= ["$scope","$http","SweetAlert"];

    function usuarioActuCtrl($scope,$http,SweetAlert) {
        var vm = this;
        var id= document.getElementById('id').value;
        vm.u = {};
        vm.u.id = id;
        vm.cargos = [];
        vm.epss = [];
        vm.arls = [];
        vm.perfiles = [];
        vm.info = [];
        vm.cargaDatos = function () {
            $http.post('../../../../GestionRecursos/controladores/usuarioAControlador.php', {
                metodo: 'datosUsuario',
                data: id
            }).then(function (response) {
                vm.info = response.data.data[0];
                vm.u.nombre = vm.info.nombre;
                vm.u.apellido = vm.info.apellido;
                vm.u.correo = vm.info.correo;
                vm.u.estado = vm.info.activo;
                vm.u.salario = vm.info.salario_basico;
                vm.u.tel = vm.info.telefono;
                vm.u.cel = vm.info.movil;
                vm.u.dir = vm.info.direccion;
                vm.u.perfil = vm.info.perfil_id;
                vm.u.cargo = vm.info.id_cargo;
                vm.u.eps = vm.info.id_eps;
                vm.u.arl = vm.info.id_arl;
            });

            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'cargo'
            }).then(function (response) {
                vm.cargos = response.data.data;
                console.log(vm.cargos);
            });

            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'eps'
            }).then(function (response) {
                vm.epss = response.data.data;
            });

            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'arl'
            }).then(function (response) {
                vm.arls = response.data.data;
            });
            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'perfil'
            }).then(function (response) {
                vm.perfiles = response.data.data;
            });
        };

        vm.actualizarUsuario= function () {
            $http.post('../../../../GestionRecursos/controladores/usuarioAControlador.php', {
                metodo: 'actualizarUsuario',
                data:  vm.u
            }).then(function (response) {
                var response = response.data;
                if(response.stado===1){
                    SweetAlert.swal(
                        response.msg,
                        "Datos actualizados!",
                        "success"
                    );
                }else{
                    SweetAlert.swal(response.msg,
                        "Alerta!",
                        "error"
                    );
                }

            });
        }
    }

})();
