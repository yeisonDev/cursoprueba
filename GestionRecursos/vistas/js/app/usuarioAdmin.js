(function () {
    'use strict';
angular
    .module('myApp')
      .controller('usuarioAdminCtrl',usuarioAdminCtrl);
    usuarioAdminCtrl.$inject= ["$scope","$http","SweetAlert"];
    function usuarioAdminCtrl($scope,$http,SweetAlert) {
     var vm = this;
        vm.u = {};
        vm.cargos = [];
        vm.epss = [];
        vm.arls = [];
        vm.perfiles = [];
        vm.listados = [];
        vm.cargaDatos= function () {
            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'cargo'
            }).then(function(response) {
                vm.cargos=response.data.data;
                console.log(vm.cargos);
            });

            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'eps'
            }).then(function(response) {
                vm.epss=response.data.data;
            });

            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'arl'
            }).then(function(response) {
                vm.arls=response.data.data;
            });
            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'perfil'
            }).then(function(response) {
                vm.perfiles=response.data.data;
            });

            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'lista'
            }).then(function(response) {
                vm.listados=response.data.data;
            });
        };

        vm.crearUsuario= function () {
            $http.post('../../../../GestionRecursos/controladores/usuarioAdminControlador.php', {
                metodo: 'crearUsuario',
                data:  vm.u
            }).then(function (response) {
               var response = response.data;
                if(response.stado===1){
                    SweetAlert.swal(
                        response.msg,
                        "Datos actualizados!",
                        "success"
                    );
                }else{
                    SweetAlert.swal(response.msg,
                        "Alerta!",
                        "error"
                    );
                }

            });
        }

    }
})();
