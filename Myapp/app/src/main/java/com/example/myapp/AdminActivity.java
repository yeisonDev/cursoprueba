package com.example.myapp;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AdminActivity extends AppCompatActivity {
//    FirebaseFirestore db;
//    CollectionReference productosref;
   EditText etInicio,etFin;
//    ListView lvProductos;
//    ArrayList<Producto> lista= new ArrayList<>();
    ArrayAdapter adapter;

    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        // Access a Cloud Firestore instance from your Activity

//       db = FirebaseFirestore.getInstance();
//        productosref = db.collection("productos");
    etInicio=findViewById(R.id.etInicio);
//        etFin=findViewById(R.id.etFin);
//        lvProductos=findViewById(R.id.lvProductos);
//        adapter= new ArrayAdapter(this,android.R.layout.simple_list_item_1,lista);
//        lvProductos.setAdapter(adapter);



    }

    public void buscar(View view) {

//        db.collection("productos")
//                .whereGreaterThanOrEqualTo("valor",etInicio.getText().toString())
//                .whereLessThanOrEqualTo("valor",etFin.getText().toString())
//                .get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful()) {
//                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                Log.d("cesde", document.getId() + " => " + document.getData().get("sabor").toString());
//                                lista.add(new Producto(document.getData().get("sabor").toString(),
//                                        Integer.parseInt(document.getData().get("valor").toString())));
//                            }
//                        } else {
//                            Log.w("cesde", "Error getting documents.", task.getException());
//                        }
//                    }
//                });

        Calendar calendar= Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);
        final int day =calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog= new DatePickerDialog(AdminActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etInicio.setText(year + "/"+ month + "/" + day);
            }
        },year,month,day);
        datePickerDialog.show();
    }


}
