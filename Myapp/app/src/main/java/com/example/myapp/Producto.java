package com.example.myapp;


import java.io.Serializable;

public class Producto implements Serializable {
    public String nombre;
    public Integer valor;

     public Producto(){}

    public Producto(String nombre, Integer valor) {
        this.nombre = nombre;
        this.valor = valor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

}
