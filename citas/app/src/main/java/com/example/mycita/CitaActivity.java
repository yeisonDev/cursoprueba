package com.example.mycita;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CitaActivity extends AppCompatActivity implements ConsultarFragment.OnFragmentInteractionListener,SoliciitarFragment
        .OnFragmentInteractionListener {

    ConsultarFragment consultarFragment;
    SoliciitarFragment soliciitarFragment;
    Button btnConsulta, btnInsertar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cita);

        consultarFragment= new ConsultarFragment();
        soliciitarFragment= new SoliciitarFragment();

        btnConsulta= findViewById(R.id.btnConsulta);
        btnInsertar= findViewById(R.id.btnIngresar);
        getSupportFragmentManager().beginTransaction().add(R.id.contenedor,consultarFragment).commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void cambiarFragment(View view) {
        Intent intent2 = getIntent();
        String usuario = intent2.getStringExtra("correo");
        Bundle bundle = new Bundle();
        bundle.putString("correo",usuario);
        FragmentTransaction fragmentTransaction= getSupportFragmentManager().beginTransaction();
        switch (view.getId()) {
            case R.id.btnConsulta:
                consultarFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.contenedor,consultarFragment).commit();
                break;
            case R.id.btnSolicitar:
                soliciitarFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.contenedor,soliciitarFragment).commit();
                break;

        }
    }


}
