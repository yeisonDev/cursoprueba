package com.example.mycita;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Calendar;

public class ConsultaActivity extends AppCompatActivity {
    FirebaseFirestore db;
    DatePickerDialog datePickerDialog;
    EditText etDesde,etHasta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);
        etDesde=findViewById(R.id.etDesde);

        Bundle params= getIntent().getExtras();
        String email = params.getString("email");
        db = FirebaseFirestore.getInstance();

        db.collection("cita")
                .whereEqualTo("correo",email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("cesde", document.getId() + " => " + document.getData());
                                Toast.makeText(ConsultaActivity.this,"Fecha: "+ document.get("fecha").toString() + " Hora: "+document.get("hora").toString() , Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d("cesde", "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    public void Consultar(View view) {

        Bundle params= getIntent().getExtras();
        String email = params.getString("email");
        db = FirebaseFirestore.getInstance();

        db.collection("cita")
                .whereEqualTo("correo",email)
                .whereEqualTo("fecha", etDesde.getText().toString())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("cesde", document.getId() + " => " + document.getData());
                                Toast.makeText(ConsultaActivity.this,"Fecha: "+ document.get("fecha").toString() + " Hora: "+document.get("hora").toString() , Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d("cesde", "Error getting documents: ", task.getException());
                        }
                    }
                });


    }

    public void desde(View view) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog= new DatePickerDialog(ConsultaActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etDesde.setText( year+ "/" + (month + 1 )+ "/" + dayOfMonth) ;
            }
        }, year,month,day);
        datePickerDialog.show();
    }

}
