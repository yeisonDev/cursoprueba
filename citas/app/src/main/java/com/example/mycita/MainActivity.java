package com.example.mycita;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {
    TextView etUser,etPass;
    private FirebaseAuth mAuth;
// ...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();


    }

    public void ingresar(View view) {
        etUser=findViewById(R.id.etUser);
        etPass=findViewById(R.id.etPass);
        String usuario= etUser.getText().toString();
        String pass= etPass.getText().toString();

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(usuario).matches()){
            etUser.setError("debe ingresar un email valido");
            return;
        }

        if (TextUtils.isEmpty(etUser.getText()) && TextUtils.isEmpty(etPass.getText())){
            Toast.makeText(this, "Indicar todos los campos", Toast.LENGTH_SHORT).show();
           return;
        }
        mAuth.signInWithEmailAndPassword(usuario, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(MainActivity.this, "Usuario correcto", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(),CitaActivity.class);
                            intent.putExtra("correo",etUser.getText().toString());
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(MainActivity.this, "Ingreso incorrecto",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }

    public void registrar(View view) {
        Intent intent1 = new Intent(getApplicationContext(),RegistroActivity.class);
        startActivity(intent1);
        finish();
    }
}
