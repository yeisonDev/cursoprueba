package com.example.mycita;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;

public class RegistroActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;

    EditText etUsuario, etContra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        mAuth = FirebaseAuth.getInstance();
    }

    public void regitrar(View view) {

        etUsuario= findViewById(R.id.etUsuario);
        etContra= findViewById(R.id.etContra);

        String user= etUsuario.getText().toString();
        String contras=etContra.getText().toString();
        if(TextUtils.isEmpty(user) ||TextUtils.isEmpty(contras) ){
            Toast.makeText(this, "Debe ingresar todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(user).matches()){
            etUsuario.setError("debe ingresar un email valido");
            return;
        }

        mAuth.createUserWithEmailAndPassword(user, contras)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(RegistroActivity.this, "Usuario registrado correctamente", Toast.LENGTH_SHORT).show();
                            Intent intent= new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(RegistroActivity.this, "Alerta! registro sin éxito - "+  task.getException(), Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }
}
