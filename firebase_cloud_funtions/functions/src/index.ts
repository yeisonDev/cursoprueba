// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

exports.addMessage = functions.https.onRequest((req:any, res:any) => {
    // Grab the text parameter.
    
    return admin.firestore().collection('productos').add({
     nombre:req.query.nombre,
     correo: req.query.correo

    }).then((r:any)=>{
      console.log("Todo correcto");
      res.send(r);
    }).catch((err:any)=>{
        console.log("error");
        res.send(err);
      });
  });


  // Listen for changes in all documents in the 'users' collection
exports.sendMessage = functions.firestore
.document('productos/{id}')
.onWrite((change:any, context:any) => {

  // The topic name can be optionally prefixed with "/topics/".
  let topic = 'hombres';

  // See documentation on defining a message payload.
  let message = {
    android: {
      ttl: 3600 * 1000, // 1 hour in milliseconds
      priority: 'normal',
      notification: {
        title: '$GOOG up 1.43% on the day',
        body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
        //icon: 'stock_ticker_update',
        color: '#f45342'
      }
    },
    topic: topic
  };
  
  // Send a message to devices subscribed to the provided topic.
   return admin.messaging().send(message)
    .then((response:any) => {
      // Response is a message ID string.
      console.log('Successfully sent message:', response);
    })
    .catch((error:any) => {
      console.log('Error sending message:', error);
    });
  
});