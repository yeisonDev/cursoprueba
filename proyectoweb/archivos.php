<?php
/*
vista central de archivos
*/
require("incluidos/sessiones.php");
include("clases/archivos.php");
$archivos = new Archivos();
// si se selecciona un archivo y se presiona el boton "subir archivo", podemos capturarlo por el metodo post e invocar la funcion cargar
if (count($_POST)>0) {
  $mensaje=$archivos->cargar();
}

if (count($_GET)>0) { 
  $mensaje=$archivos->eliminar();
}

$total_registros=$archivos->listar();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Listado de Archivos</title>
<?php include("incluidos/head.php");?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">	

<link rel="stylesheet" type="text/css" href="pace/pace.css">
<script type="text/javascript" src="pace/pace.min.js"></script>

</head>
<body>
<?php include("incluidos/menu.php");?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="principal.php">Principal</a></li>
    <li class="breadcrumb-item active" aria-current="page">Listado de archivos</li>
  </ol>
</nav>
<?php
if (isset($mensaje)) { ?>
  <div class="row">
    <?php echo $mensaje;?>
  </div>  
<?php
}
?>
<br>
<div class="row">
 <a href="#" class="btn btn-success" onclick="abrir();">Nuevo Registro</a>
</div>
<br>
<div class="row" id="capa_archivo" style="display: none">
<form method="post" name="frm" id="frm" action="" enctype="multipart/form-data">
  <input type="file" name="archivo[]" id="archivo" ><br>
  <input type="file" name="archivo[]" id="archivo" >

  <button type="submit" name="enviar" id="enviar" class="btn btn-info">Subir Archivo</button>
  <br>
  (Tamaño maximo permitido: <?php echo ini_get('post_max_size')?>)
</form>
<br>
</div>
<div class="row">
  <div class="col-sm-12">
 

  <table class="table" id="data-archivos">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Tipo de Archivo</th>
        <th>Tamaño</th>
        <th>Op</th>
      </tr>
    </thead>
<tbody>
  <?php 
  $contador=0;
    foreach($total_registros as $vector) {
      $contador++;
      ?>
  <tr >
        <th scope="row"><?php 
        // convertir la imagen a base64 para que nadie conozca el origen de la informacion
        // se usa file_get_contents combinado con base64_encode
        $img=$archivos->cargarruta()."".$vector["nombre"];
        $tipo=strtolower($vector["tipo"]);
        // la imagen se codifica asi:
        // data: para definir que lo lea el src del tag img
        // base64 para que sepa que va codificado
        // file_get_content: lee un contenido pasando la ruta y el nombre del contenido

        $imgcodificada='data:'.$tipo.";base64,".base64_encode(file_get_contents($img));


          if ($tipo=="jpg" || $tipo=="png" || $tipo=="gif" || $tipo=="svg" || $tipo=="jpeg") {
            ?>
            <a href="#" data-toggle='modal' data-target='#ventanaimagen' onclick="cargarimagen('img-<?php echo $contador;?>')" title='Click para ampliar la imagen'><img id="img-<?php echo $contador;?>" src="<?php echo $imgcodificada;?>" width='150'></a>
            <?php
          } else {
          echo $vector["nombre"];
            
          }

        ?></th>
        <td><?php echo $vector["tipo"]?></td>
        <td><?php echo number_format($vector["peso"]/1024/1024,2);?> MB</td>
        <td>
          <a href="descargar.php?nombre=<?php echo base64_encode($vector["nombre"]);?>" class="btn btn-dark">Descargar</a>
          <a href="?nombre=<?php echo base64_encode($vector["nombre"]);?>" class="btn btn-danger">Eliminar</a>
        </td>
 </tr>
      <?php
    }?>
</tbody>
  </table>
</div>

</div>

<?php include("incluidos/js.php");?>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	
	$(document).ready( function () {
    $('#data-archivos').DataTable(
    {
    	"language": { 
    		//"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    	}
    }

    	);
    
    
	} );

function abrir() {
      // $("#capa_archivo").fadeIn(3000);
      $("#capa_archivo").show();

    }

    function cargarimagen(imagen) {
          // capturar el src de la imagen y se la vamos a pasar al elemento data-img que esta en el modal
          var valor=$("#"+imagen).attr('src');
          $("#data-img").attr('src',valor);
    }

</script>
</body>
</html>

<div class="modal fade" id="ventanaimagen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content"  style="width: 650px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Imagen Ampliada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="" id="data-img" width="500">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



