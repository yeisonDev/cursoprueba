<?php
/*
clase archivos
Esta clase permite varios procesos:
1. determinar cual sera la carpeta en la cual cargaremos los archivos
2. proceso para cargar la ruta en caso que la necesitemos en la vista
3. proceso de listar archivos basados en la ruta
4. cargar archivos basados en el formulario y la ruta
5. eliminar archivos basafos en la ruta asociada
*/
include("conexion.php");
class Archivos extends Conexion { 
	function __construct() {
		parent:: __construct();
		$this->ruta="assets/data/";
	}		
	function cargarruta() {
		return $this->ruta;
	}
	function listar() {
		// esta funcion o metodo permite carpturar los archivos en la  ruta y extraer sus propiedades internas para mostrarlas en la vista
		$listararchivos=Array();

		// para poder leeer una carpeta se puedehacer de varias maneras
		// aca vamos a utilizar lo siguiente:
		//1. is_dir: permite saber si la carpeta existe
		//2 . opendir: permite abrir un apuntador o lector de la carpeta4
		//3. readdir permite leer tod el contenido de la carpeta
		//4. closedir_: cierra el apuntador o lector creado en opendir
		if (is_dir($this->ruta)) {
			$directorio=opendir($this->ruta);

			while ($archivo=readdir($directorio)) {

				// en la variable $archivo podemos extraer el nombre, tipo y tamaño
				// como estamos en un while, php tambien captura las rutas relativas . y .. por lo cual
				// debemos omitirlas
				if ($archivo<>"." && $archivo<>"..") {
					$nombre=$archivo;
					$tipo=pathinfo($archivo,PATHINFO_EXTENSION);
					$peso=filesize($this->ruta.$archivo);
					$listararchivos[]=array("nombre"=>$nombre,"tipo"=>$tipo,"peso"=>$peso);
				}

			}

			closedir($directorio);
		} 

		return $listararchivos;
	}

	function cargar(){
		// esta funcion permite cargar archivos, basados en el metodo de PHP conocido como $_FILES el cual se activa en la etqueta form enctype = multipart/form-data
		//print_r($_FILES);
		//print_r($_FILES);
		$mensaje="<div class='alert alert-warning'>";
		$subidos="";
		$nosubidos="";

		for ($i=0; $i <count($_FILES["archivo"]["name"]) ; $i++) { 
			if ($_FILES['archivo']['name'][$i]<>"") {
				$origen=$_FILES['archivo']['tmp_name'][$i];
				$destino=date("YmdHis")."-".$_FILES['archivo']['name'][$i];
				if (move_uploaded_file($origen,$this->ruta.$destino)) {
					$subidos.=$origen."<br>";
				} else {
					$nosubidos.=$origen."<br>";
				}				


			}	
		}
		if ($subidos<>"") { 
			$mensaje.="<br>Archivos cargados: ".$subidos;
		}
		if ($nosubidos<>"") { 
			$mensaje.="<br>Archivos sin cargar: ".$nosubidos;
		}

		$mensaje.="</div>";
			
		
		
		/*if ($_FILES['archivo']['name']<>"") {

			// usaremos la funcion move_upload_file la cual necesita dos variables:
			// 1. el archivo origen
			// 2. la ruta + el nombre deseado para ubicar en la carpeta
			// cuando se ejecuta devuelve verdadero o falso
			$origen=$_FILES['archivo']['tmp_name'];
			$destino=date("YmdHis")."-".$_FILES['archivo']['name'];

			if (move_uploaded_file($origen,$this->ruta.$destino)) {
				$mensaje="<div class='alert alert-success'>Archivo cargado al sistema</div>";
			} else {
				$mensaje="<div class='alert alert-warning'>El Archivo no se puede cargar, verifique el tipo o el tamaño de este.</div>";
			}

		} else {
			$mensaje="<div class='alert alert-danger'>Seleccione un archivo para subirlo al sistema</div>";
		}*/

		return $mensaje;
	}

	function eliminar() {
		$nombre=base64_decode($_GET['nombre']);
		// para eliminar una imagen preguntamos si existe y luego le aplicamos la funcion unlink
		if (is_file($this->ruta.$nombre)) {

			unlink($this->ruta.$nombre);

			$mensaje="<div class='alert alert-success'>Archivo Eliminado del sistema</div>";


		} else {
			$mensaje="<div class='alert alert-danger'>La imagen no existe y no se puede borrar del sistema.</div>";

		}

		return $mensaje;
	}

}
?>