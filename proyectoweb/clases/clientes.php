<?php
/*
clase clientes
*/
include_once("conexion.php");
class Clientes extends Conexion { 
	function __construct() {

		parent:: __construct();
		$this->tabla="tblclientes";
		// instanciar las varibles del formulario que se envia desde el ajax
		if (count($_POST)>0) {
			$this->nombres=$_POST['nombres'];
			$this->apellidos=$_POST['apellidos'];
			$this->movil=$_POST['movil'];
			$this->correo=$_POST['correo'];

		} 
	}		

	// listado de registros
	function listar() {
		$sql="select * from ".$this->tabla;
		$data=array();
		// recorrer con un while los datos del select y vamos a retornar un array;
		$resultado=$this->conexion->query($sql);
		while($fila=$resultado->fetch_array()) {
			$data[]=$fila;
		}		
		return $data;
	}

	function ingreso() {

		// construir una variable que me guarde la ejecucion del sql
		$sql="insert into ".$this->tabla;
		$sql.=" (";
		$sql.=" nombres,apellidos,movil,correo";
		$sql.=" )";
		$sql.=" values ";
		$sql.=" (";
		$sql.="'".$this->nombres."',";
		$sql.="'".$this->apellidos."',";
		$sql.="'".$this->movil."',";
		$sql.="'".$this->correo."'";
		$sql.=" )";
		
		if ($this->conexion->query($sql)) {
			$proceso=1; // lo pudo insertar
		} else {
			// por el else se puede encontrar o un error de sintaxis o si la tabla tiene clave unica el sistema me devuelve la palabra duplicatew
			$error=$this->conexion->error;
			$palabra=strrpos($error, "Duplicate");
			if ($palabra===false) {
				$proceso=3; // problemas con la insercion
			} else {
				$proceso=2; // el registro se encuentra duplicado
			}
		
		}

		return $proceso;


	}

	function detalle() {
		$registro=Array();
		if (count($_GET)>0) {
			$id=base64_decode($_GET['id']);
			$sql="select * from ".$this->tabla." where id=".$id;
			$resultado=$this->conexion->query($sql);
			if ($resultado->num_rows>0) {
				$registro=$resultado->fetch_Array();
			}
		}

		return $registro;
	}

	function actualizar() {
		$id=base64_decode($_POST['id']);

		$sql=" update ".$this->tabla." set ";
		$sql.="nombres='".$this->nombres."'";
		$sql.=",apellidos='".$this->apellidos."'";
		$sql.=",movil='".$this->movil."'";
		$sql.=" where id=".$id;
		if ($this->conexion->query($sql)) {
			$registro=4;
		} else {
			$registro=3;
		}

		return $registro;

	}
	//  proceso de eliminacion
	function eliminar() {
			$id=base64_decode($_GET['id']);
			$sql="delete from ".$this->tabla;
			$sql.=" where id =".$id;
			if ($this->conexion->query($sql)) {
				$registro=1; // se pudo borrar
			} else {
				$registro=0; // se presento un problema al tratar de borrar el registro
			}
		return $registro;	
	}
}
?>








