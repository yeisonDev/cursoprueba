<?php 
/*
Este script almacena las variables de conexion al servidor y activa el conector hacia la base de datos
para este usaremos el constructor para que la puedan heredar las otras clases sin necesidad de invocar funciones adicionales
*/
/**
* 
*/
class Conexion 
{
	
	function __construct()
	{
		// instanciar las variables de conexion hacia la base datos
		$this->servidor="localhost";
		$this->usuario="root";
		$this->clave="";
		$this->basededatos="desarrolloweb122018";
		// crear la conexion que vamos a utilizar en el proyecto
		$this->conexion=new mysqli($this->servidor,$this->usuario,$this->clave,$this->basededatos);
		// validar si la conexion se realiza o no
		if ($this->conexion->connect_errno) {
			die("Fallo al conectar a la base de datos.");
		} 

	}
}

//file_get_contents('php //input')
?>

