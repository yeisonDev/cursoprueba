<?php
/*
clase usuarios
Esta clase administra todos los procesos alrededor de los usuarios:
1. acceso al sistema
2. listar
3. ingresar
4. modificaer
5. eliminar
Esta clase hereda las propiedades y el metodo constructor de conexion
Con el fin de no estar en cada clase que creemos invocando la conexion porque para eso conexion.php la tiene instanciada en su constructor

*/

include_once("conexion.php");
class Usuarios extends Conexion { 
	function __construct() {

		parent:: __construct();
		// cargar como variable global de la clase la tabla de usuarios
		$this->tabla="productos";

			
			$this->id= $_POST['id'];
			$this->producto=$_POST['producto'];
			$this->valor=$_POST['valor'];
			$this->categoria=$_POST['categoria'];
			$this->descripcion=$_POST['descripcion'];
			
		

	}		
     

    function  acceso() {
		// esta funcion permite validar el acceso al sistema
		//  1. recuperemos los datos desde el formulario index.php
		$usuario=$_POST['usuario'];
		$clave=$_POST['clave'];
		// 2. construir el query que nos traer el registro de la base de datos 
		$sql="select  * from ".$this->tabla." where correo='$usuario' ";
		//3. crear una variable tipo vector que almacenara los datos del usuario paea extraerlos en validar.php
		$registro=array();
		if ($resultado=$this->conexion->query($sql)) {
			// 4. preguntar si traer registros
			if ($resultado->num_rows>0) {
				// 5. recuperar el registro para extraer la contraseña guardada
				// y compararla con la digitada en la variable clave
				$vector=$resultado->fetch_array();
				// 6. extraer la clave
				$claveacomparar=$vector["clave"];
				// 7. comparar ambas clave para darle acceso al usuario
				$comparar=password_verify($clave,$claveacomparar);
				
				if ($comparar) { 
					$registro=$vector;
				} 
			}
		}
		return $registro;
	}


	// listado de Usuarios
	function listar() {
		$sql="select * from ".$this->tabla;
		$data=array();
		// recorrer con un while los datos del select y vamos a retornar un array;
		$resultado=$this->conexion->query($sql);
		while($fila=$resultado->fetch_array()) {
			$data[]=$fila;
		}		
		return $data;
	}

	function detalle(){
          $registro=Array();
		if (count($_GET)>0) {
			$id=base64_decode($_GET['id']);
			$sql="select * from ".$this->tabla." where id=".$id;
			$resultado=$this->conexion->query($sql);
			if ($resultado->num_rows>0) {
				$registro=$resultado->fetch_Array();
			}
		}

		return $registro;
	}



      function actualizar() {
		$id=base64_decode($_POST['id']);
		$sql="update ".$this->tabla." set ";
		$sql.="nombres='".$this->nombres."'";
		$sql.=",apellidos='".$this->apellidos."'";
		$sql.=",correo='".$this->correo."'";
		$sql.=" where id=".$id;
		if ($this->conexion->query($sql)) {
			$registro=1;
		} else {
			$registro=2;
		}
     	return $registro;

	    }
	

	function ingreso(){
    // construir una variable que me guarde la ejecucion del sql
		$sql="insert into productos (id ,producto ,valor ,categoria,Descripcion)
	        values ( ". $this->id . ", '" .$this->producto . "', " .$this->valor. ",'". $this->categoria ."','". $this->descripcion. "')";


		if ($this->conexion->query($sql)) {
			$proceso=1; // lo pudo insertar
		} else {
			// por el else se puede encontrar o un error de sintaxis o si la tabla tiene clave unica el sistema me devuelve la palabra duplicatew
			$error=$this->conexion->error;
			$palabra=strrpos($error, "Duplicate");
			if ($palabra===false) {
				$proceso=3; // problemas con la insercion
			} else {
				$proceso=2; // el registro se encuentra duplicado
	 		}
		
		}
     	return $sql;		

	}


		//  proceso de eliminacion
	function eliminar() {
			$id=base64_decode($_GET['id']);
			$sql="delete from ".$this->tabla;
			$sql.=" where id =".$id;
			if ($this->conexion->query($sql)) {
				$registro=1; // se pudo borrar
			} else {
				$registro=0; // se presento un problema al tratar de borrar el registro
			}
		return $_POST['id'];	
	}
}
