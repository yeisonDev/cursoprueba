<?php
/*
EJEMPLO 3 DE CLASES
Herencia de clases
una clase se puede heredar de otra usando la palabra extends
Condiciones:
- la clase padre o que hereda debe estar antes de la invocacion
*/
include("class2.php");
class Vehiculo extends Carro {

	function __construct() {
		// si usamos aca el constructor sobreescribe el constructor que heredamos. Para traernos el constructor padre, parent
		parent:: __construct();
	}


	function velocidad($distancia) {
		$resp=$distancia/$this->velocidad;
		return $resp;
	}


}
$obj=new Vehiculo;
$resp=$obj->velocidad(700);
echo "El tiempo esperado es: ".$resp." Horas";