<?php
/*
Script que recibe las peticiones del ajax o cualquier otra fuente pero con la condicion que lo datos siempre deben ser en metodo post
*/
include("clases/clientes.php");
$clientes=new Clientes;
// preguntar si se esta pasando el id
if (isset($_POST['id'])) { 
	$proceso=$clientes->actualizar();
} else {
	$proceso=$clientes->ingreso();
}
switch ($proceso) {
	case '1':
		echo "<span class='btn btn-success'>El registro ha sido ingresado</span>";
		break;
	case '2':
		echo "<span class='btn btn-warning'>El registro ya se encuentra en el sistema. Intente de nuevo</span>";
		break;
	case '4':
		echo "<span class='btn btn-success'>El registro ha sido actualizado</span>";
		break;

	default:
		echo "<span class='btn btn-danger'>Hay un error en el proceso con la base de datos.</span>";
		break;
}
?>