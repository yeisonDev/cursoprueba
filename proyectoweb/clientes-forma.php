<?php
/*
vista central del formulario de clientes que nos sirve tanto 
para ingresar como modificar datsos
todos los procesos se haran por medio de ajax
*/
require("incluidos/sessiones.php");
// si se pasa el parametro para modificar, invocamos la clase y vamos a crear una nueva funcion
// que se llame detalle. la cual nos va a retorna un vector con los campos y sus respectivos valores
// se comporta de manera similar al listar
if (count($_GET)>0) {
  include("clases/clientes.php");
  $data=new Clientes();
  $resultado=$data->detalle();

}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Registro de datos</title>
<?php include("incluidos/head.php");?>
<link rel="stylesheet" type="text/css" href="pace/pace.css">
<script type="text/javascript" src="pace/pace.min.js"></script>
</head>
<body>
<?php include("incluidos/menu.php");?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="principal.php">Principal</a></li>
    <li class="breadcrumb-item"><a href="clientes.php">Listado de clientes</a></li>
    <li class="breadcrumb-item active" aria-current="page">Registro de datos</li>
  </ol>
</nav>
<div class="row">
 <a href="clientes.php" class="btn btn-success">Regresar</a>
</div>
<div class="row">
  <div class="col-sm-12">

<form id="frm" name="frm" method="post" action="">
   <div class="form-group row">
      <label for="nombres" class="col-sm-2 col-form-label">Nombres</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="nombres" name="nombres" placeholder="nombre completo " autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["nombres"]?>">
      </div>
    </div>
  <div class="form-group row">
      <label for="apellidos" class="col-sm-2 col-form-label">Apellidos</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="apellidos " autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["apellidos"]?>">
      </div>
    </div>
  <div class="form-group row">
      <label for="movil" class="col-sm-2 col-form-label">Movil</label>
      <div class="col-sm-9">
        <input type="number" class="form-control" id="movil" name="movil" placeholder="Movil " autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["movil"]?>">
      </div>
    </div>
    <div class="form-group row">
      <label for="correo" class="col-sm-2 col-form-label">Correo electrónico</label>
      <div class="col-sm-9">
        <input type="email" class="form-control" id="correo" name="correo" placeholder="Email" autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["correo"]?>" <?php if (isset($resultado)) echo "readonly";?> >
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-9">
<?php if (isset($resultado)) {?>
        <button type="submit" class="btn btn-primary">Actualizar</button>
<?php } else {?>
        <button type="submit" class="btn btn-primary">Ingresar</button>
<?php } ?>

      </div>
    </div>

  <div id="mensaje"></div>

<?php if (isset($resultado)) {?>
<input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>">
<?php }?>


  </form>


</div>
</div>
<?php include("incluidos/js.php");?>
<script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    //invocar el ajax junto con sus parametros
    $("#frm").submit(function(evento){
        evento.preventDefault();
        // para no estar invocando todos los parametros a pasar, usamos serialize()
        // lo que hace es que organiza todos los campos con sus valores en un array de manera
        // automatica
        var parametros=$("#frm").serialize();

        $.ajax({
        data : parametros,
        url: "clientes-data.php",
        type: "post",
        beforesend : function () {
            $("#mensaje").html("<span class='btn btn-warning'>Cargando información...</span>");
        },
        success : function (response) {
            $("#mensaje").show();
            $("#mensaje").html(response);
            $("#mensaje").fadeOut(5000);
        },
        error : function (jqXHR,textStatus,errorThrown) {
          $("#mensaje").html("<span class='btn btn-danger'>Se ha presentado un error: "+errorThrown+" : "+textStatus+"</span>");
        } 

        });

    }); 
});   

</script>
</body>
</html>




