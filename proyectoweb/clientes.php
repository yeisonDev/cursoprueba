<?php
/*
vista central de clientes
*/
require("incluidos/sessiones.php");
include("clases/clientes.php");
$clientes=new Clientes;
// preguntar si se esta pasando un parametro por el metodo get
if (count($_GET)>0) {
  // se paso el id que viene del boton eliminar
  $proceso=$clientes->eliminar();
  if ($proceso==1) { 
      $mensaje="<div class='alert alert-success'>El registro ha sido eliminado con exito</div>";
  } elseif ($proceso==0) {
      $mensaje="<div class='alert alert-danger'>El proceso no se puede realizar. Intente de nuevo</div>";

  }
}

$total_clientes=$clientes->listar();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Principal del sistema</title>
<?php include("incluidos/head.php");?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">	

<link rel="stylesheet" type="text/css" href="pace/pace.css">
<script type="text/javascript" src="pace/pace.min.js"></script>

</head>
<body>
<?php include("incluidos/menu.php");?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="principal.php">Principal</a></li>
    <li class="breadcrumb-item active" aria-current="page">Listado de clientes</li>
  </ol>
</nav>
<?php
if (isset($mensaje)) { ?>
  <div class="row">
    <?php echo $mensaje;?>
  </div>  
<?php
}
?>

<div class="row">
 <a href="clientes-forma.php" class="btn btn-success">Nuevo Registro</a>
</div>
<br>
<div class="row">
  <div class="col-sm-12">
 

  <table class="table" id="data-clientes">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Movil</th>
        <th>Correo</th>
        <th>Fecha de ingreso</th>
        <th>Ultima modificacion</th>
        <th>Opciones</th>
      </tr>
    </thead>
<tbody>
  <?php 
    foreach($total_clientes as $vector) {
      ?>
  <tr ">
        <th scope="row"><?php echo $vector["id"];?></th>
        <td><?php echo $vector["nombres"];?></td>
        <td><?php echo $vector["apellidos"];?></td>
        <td><?php echo $vector["movil"];?></td>
        <td><?php echo $vector["correo"];?></td>
        <td><?php echo $vector["fechaingreso"];?></td>
        <td><?php echo $vector["fechamodificacion"];?></td>
        <td>
          <a href="clientes-forma.php?id=<?php echo base64_encode($vector["id"]);?>" class="btn btn-info">Modificar</a>
          <a href="clientes.php?id=<?php echo base64_encode($vector["id"]);?>" class="btn btn-danger">Eliminar</a>
        </td>
 </tr>
      <?php
    }?>
</tbody>
  </table>
</div>

</div>

<?php include("incluidos/js.php");?>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	
	$(document).ready( function () {
    $('#data-clientes').DataTable(
    {
    	"language": { 
    		//"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    	}
    }

    	);
    
	} );


</script>
</body>
</html>




