<?php
/*
Archivo descargar.ph
Para que funcione este archivo se debe hacer:
1. validar que se pase el parametro de la imagen
2. cargar desde la clase archivo la ruta para no tener que quemarla en este archivo
3. aplicar los headers o encabezados del html para forzar dicha descarga
*/
if (count($_GET)>0) {
  // recuperar los valores
  // traer la ruta y preguntar si el archivo existe usando is_file
  $nombre=base64_decode($_GET['nombre']);
  include("clases/archivos.php");
  $data=new Archivos;
  $ruta=$data->cargarruta();
  $archivo=$ruta.$nombre;
  if (is_file($archivo)) {
    // proceso de descarga
    // 1. indicarle al archivo que se va a forza la descarga 
    header("Content-type: Application/force-download");
    //header("Content-type: Image/png");
    //2. indicarle al aarchivo que se va descarga un documento o elemento tipo binario
    // y que le este indicando cuanto tiempo de descarga para conocer  la duraccion de bajar la informacion
    header("Content-type: Application/octet-stream");
    // 3. indicarle al archivo vamos a adjuntar el documento o elemmento binario
    header("Content-Disposition: attachment;filename=".rawurlencode($nombre));
    // 4. indicarle al archivo que se va transmitir un deocumento binario
    header("Content-Transfer-Encoding: binary");
    // 5. indicarle al archivo el tamaño del documento o elemento binario
    header("Content-Length:".filesize($archivo));
    // 6. lea el archivo o elemento binario
    readfile($archivo);
  } else {
    die("Archivo no encontrado.<a href='archivos.php'>Regresar</a>");
    exit();
  }
} else {
  die("Archivo no enviado.<a href='archivos.php'>Regresar</a>");
  exit();
}