<?php
/*
EJEMPLO 1 DE CLASES
en php se denota con la palabra class
- se pueden crear variable, constructor
- funciones privadas, protegidas, publicas
- las clases pueden heredar de otras clases

**** Por nomenclaturas los nombres de las clases siempre comienzan con la primera letra en mayuscula
*/
class Carro {
	// crear funcion encendido
	// si la funcion no tiene indicada la forma de invocarla en la clase, php asume que la funcion es publica, de acceso afuera de la clase
	function encendido($param) {
		switch ($param) {
			case 'On':
				$resp="Vehiculo prendido";
				break;
			
			default:
				$resp="Vehiculo apagado";
				break;
		}
		return $resp;
	}
}
// invocar la clase
$obj=new Carro;
// extraer un metodo de la clase
$encendido=$obj->encendido('On');
// imprimir resultado
echo "<h2>".$encendido."</h2>";