<?php
/*
EJEMPLO 2 DE CLASES
// METODO CONSTRUCTOR
esta funcion permite iniciar variables, realizar operaciones, conexiones a las base de datos o cualquier proceso que se pueda usar
DENTRO DE LA CLASE o que se pueda HEREDAR En otra.

*/
class Carro {
	function __construct() {
		// crear una variable que almacen el costo de kilometraje.
		// las variables se inicializan anteponiendo this
		$this->costokilo=10000;
		$this->velocidad=70;
	}

	function encendido($param) {
		switch ($param) {
			case 'On':
				$resp="Vehiculo prendido";
				break;
			
			default:
				$resp="Vehiculo apagado";
				break;
		}
		return $resp;
	}

	function consumo($kilometraje) {
		// como la variable costokilo esta en el contructor, 
		// se puede cargar encualquier parte de a case y sus clases que lo hereden
		$valor=$kilometraje*$this->costokilo;
		return $valor;
	}
}
/*$obj=new Carro;
$consumo=$obj->consumo(250.52);
echo "<h2>Valor consumo: ".number_format($consumo,2)."</h2>";*/