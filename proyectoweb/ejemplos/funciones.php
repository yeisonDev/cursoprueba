<?php
/*
ejemplos de funciones
//1. funcion con datos que son obligatorios
*/
function operaciones($a,$b,$operador){
	// dependiendo del tipo de operador por medio de condicionales
	// realizamos sumas, restas, divisiones o mutiplicaciones
	if ($operador=="+") {
		$resultado=$a+$b;
	}elseif ($operador=="-") {
		$resultado=$a-$b;
	}elseif ($operador=="*") {
		$resultado=$a*$b;
	}elseif ($operador=="/") {
		$resultado=$a/$b;
	} else {
		$resultado="El operador no es valido";
	}
	return $resultado;
}
echo operaciones("023",34,"/");
echo "<hr>";
// 2. funcion con datos que no sean obligatorios. Esta caracteristica se debe manejar con mucho cuidado. Normalmente se usa cuando se han creado funciones y al tiempo es necesario hacerle modificaciones tales como agregar nuevos parametros al final
function operaciones_2($a,$b,$operador="") {
	// como se sabe que son 4 operaciones en ves de usar los condicionales
	// se puede usar el concepto evaluar o case. en php se usa combinando
	// la funcion switch
	switch ($operador) {
		case '+':
			$resultado=$a+$b;
			break;
		case '-':
			$resultado=$a-$b;
			break;
		case '*':
			$resultado=$a*$b;
			break;
		case '/':
			$resultado=$a/$b;
			break;
		default:
			$resultado="El operador no es valido";
			break;
	}
	return $resultado;
} 
echo operaciones_2("23",4,"*");
// 3. cualquier tipo de funcion debe ser blindada o testeada para EVITAR 
// la menor cantidad de errores y JAMAS suponer que el cliente hara las cosas a nuestra manera
// en este caso vamos a blindar el case con el divisor
echo "<hr>";
function operaciones_3($a,$b,$operador="") {
	switch ($operador) {
		case '+':
			$resultado=$a+$b;
			break;
		case '-':
			$resultado=$a-$b;
			break;
		case '*':
			$resultado=$a*$b;
			break;
		case '/':
		// el diferente con != o con <>
		// comparadores en un if pueden ser:
		// and &&
		// or ||
			if ($b==0) {
				$resultado="El divisor no puede ser menor o igual a cero";				
			} else {
				$resultado=$a/$b;
			}
		
			break;
		default:
			$resultado="El operador no es valido";
			break;
	}
	return $resultado;
}
echo operaciones_3(23,15,"/");
echo "<hr>";
echo "<h1>Formato de resultados con operaciones matematicas</h1>";
// 1. igualar el valor en una variable
$resultado=operaciones_3(23,15,"/");
// 2. el resultado con dos decimales
echo "<br>- funcion number_format:".number_format($resultado,2);
// 3. el resultado aproximado al valor superior
echo "<br>- funcion ceil:".ceil($resultado);
// 4. el resultado aproximado al inferior
echo "<br>- funcion floor:".floor($resultado);
//5. el resultado redondeado
echo "<br>- funcion round:".round($resultado);
//6. el valor absoluto de un resultado
echo "<br>- funcion abs:".abs($resultado);
// 7. el valor llevado a su entero
echo "<br>- funcion intval:".intval($resultado);
//8 raiz cuadrada
echo "<br>- funcion sqrt:".sqrt($resultado);
//9 potencia
echo "<br>- funcion potencia:".pow($resultado,5);
















