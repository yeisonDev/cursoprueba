<?php
/* Esta funcion permite ver toda la configuracion del php */
//phpinfo();
//1. Crear una variable
$var="2";
// 1.1. definir una variable
define("variable","esto es un texto");
echo variable;
// nota: normalmente las variables que se define son las que no se mueven en un proyecto tales como conexiones a la base de datos, rutas de imagenes o variables de uso recurrente en el proyecto
// 2. impresion de resultados o variables, usemos echo, printf, print_r, var_dump. Las dos ultimas aplican a vectores o valores en formato de notacion de javascript conocido JSON
echo "<br>";
echo $var; 
// 3. concatenar variables
echo "<hr>";
$suma=2;
$var1=4;
$var2="resultado";
// la concatenacion se hace con el punto (.) y se puede imprimir o crear una VARIABLE con otros elementos unidos
$var3=$suma.$var1.$var2;
echo $var3;
// 4.  vectores
//4.1 creacion de un vector de datos usando []
$vector[]=1;
$vector[]="!hola";
$vector[]=5*4/20;
//4.2. impresion o mostrar el vector
// un valor especifico
echo "<hr>";
echo "el valor de la posicion 2 es ".$vector[2];
echo "<hr>";
// Recorrer un vector. Para esto usamos la combinacion de un ciclo y un contador de vectores. La funcion nativa de contar vectores count() y los ciclos se usa for
$total=count($vector);
for ($i=0; $i <$total ; $i++) { 
 	echo $vector[$i]."<br>"; 
 } 
 echo "<hr>";
//4.3. segunda forma de crear vectores
$vector=array("q","hora",4*6*8/52,"texto");
$total=count($vector);

for ($i=0; $i <$total ; $i++) { 
 	echo $vector[$i]."<br>"; 
 } 
// 4.4. Un vector dentro de otro vector
$vector=array("juan","fernandez","71.889.333",array("28/05/1980","Masculino","Medellin"));
echo "<hr>";
// conocer como esta formado un vector se usa print_r
print_r($vector);
echo "<hr>";
// cuando no se conoce si una variable es vector o no se pregunta por la funcion is_array()
$total=count($vector);
for ($i=0; $i <$total ; $i++) { 
	
	if (is_array($vector[$i])) {
		// generamos otro for
		// proceso:
		// declaramos en una variable el vector encontrado
		// calculamos el tamaño de la variable con un count
		// creamos un for con un subindice y el total
		$subvector=$vector[$i];
		$subtotal=count($subvector);
		for ($j=0; $j <$subtotal ; $j++) { 
			echo $subvector[$j]."<hr>";
		}
	} else {
		// se imprime el vector
		echo $vector[$i]."<br>";
	}

}
?> 















