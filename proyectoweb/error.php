<?php
/* cuando estemos en produccion y tenemos que ver un error de manera rapida usamos las dos siguientes funciones */
error_reporting(E_ALL);
ini_set("display_errors","On");

/* Esta funcion permite ver toda la configuracion del php */
//phpinfo();
//1. Crear una variable
$var="2";
// 1.1. definir una variable
define("variable","esto es un texto")
echo variable;
// nota: normalmente las variables que se define son las que no se mueven en un proyecto tales como conexiones a la base de datos, rutas de imagenes o variables de uso recurrente en el proyecto
// 2. impresion de resultados o variables, usemos echo, printf, print_r, var_dump. Las dos ultimas aplican a vectores o valores en formato de notacion de javascript conocido JSON
echo "<br>";
echo $var; 
?>  