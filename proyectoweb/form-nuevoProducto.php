<?php	
require("incluidos/sessiones.php");
?>		
<!DOCTYPE html>
<html>
<head>
	<title>Registro de datos</title>
<?php include("incluidos/head.php");?>
<link rel="stylesheet" type="text/css" href="pace/pace.css">
<script type="text/javascript" src="pace/pace.min.js"></script>
</head>
<body>
<?php include("incluidos/menu.php");?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="principal.php">Principal</a></li>
    <li class="breadcrumb-item"><a href="clientes.php">Listado de usuarios</a></li>
    <li class="breadcrumb-item active" aria-current="page">Registro de Productos</li>
  </ol>
</nav>
<div class="row">
 <a href="productos.php" class="btn btn-success">Regresar</a>
</div>
<div class="row">
  <div class="col-sm-12">

<form id="frm" name="frm" method="post" action="">
   <div class="form-group row">
      <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre del producto " autocomplete="off" required>
      </div>
    </div>
  <div class="form-group row">
      <label for="referencia" class="col-sm-2 col-form-label">Referencia</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="referencia" name="referencia" placeholder="referencia " autocomplete="off" required >
      </div>
    </div>

  <div class="form-group row">
      <label for="valor" class="col-sm-2 col-form-label">Valor</label>
      <div class="col-sm-9">
         <input type="number" class="form-control" id="valor" name="valor" placeholder="valor" autocomplete="off" required  >
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-9">
        <button type="submit" class="btn btn-primary">Ingresar</button>
      </div>
    </div>
  <div id="mensaje"><div/>
  </form>
</div>
</div>
<?php include("incluidos/js.php");?>
<script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    //invocar el ajax junto con sus parametros
    $("#frm").submit(function(evento){
        evento.preventDefault();
        // para no estar invocando todos los parametros a pasar, usamos serialize()
        // lo que hace es que organiza todos los campos con sus valores en un array de manera
        // automatica
        var parametros=$("#frm").serialize();

        $.ajax({
        data : parametros,
        url: "nuevoProducto.php",
        type: "post",
        beforesend : function () {
            $("#mensaje").html("<span class='btn btn-warning'>Cargando información...</span>");
        },
        success : function (response) {
            $("#mensaje").show();
            $("#mensaje").html(response);
            $("#mensaje").fadeOut(5000);
        },
        error : function (jqXHR,textStatus,errorThrown) {
          $("#mensaje").html("<span class='btn btn-danger'>Se ha presentado un error: "+errorThrown+" : "+textStatus+"</span>");
        } 

        });

    }); 
});   

</script>
</body>
</html>