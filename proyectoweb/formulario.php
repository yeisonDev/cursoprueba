<!DOCTYPE html>
<html>
<head>
	<title>Ejemplo de formularios</title>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
	<h1>Formulario de contactenos</h1>
<form name="form" id="form" method="post" action="validar.php">
<label for="nombres">Nombres completos</label>	
<br>
<input type="text" name="nombres" id="nombres" maxlength="50" placeholder="Digite aca su nombre" required autocomplete="off">
<br>
<label for="correo">Correo electronico</label>	
<br>
<input type="email" name="correo" id="correo" maxlength="30" placeholder="Digite aca su correo" required autocomplete="off">
<br>
<label for="telefono">Telefono de contacto</label>	
<br>
<input type="number" name="telefono" id="telefono" maxlength="13" placeholder="Digite aca su telefono" required autocomplete="off">
<br>
<label for="contactado">Como desea ser contactado</label>	
<br>
<input type="radio" name="contactado" id="contactado" value="porcorreo">Por correo
<input type="radio" name="contactado" id="contactado" value="portelefono">Por Telefono
<br>
<label for="opcion">Asunto del contacto</label>
<br>
<select name="opcion" id="opcion" required>
	<option value="">Seleccione</option>
	<option value="reclamacion">Reclamacion</option>
	<option value="solicitud">Solicitud</option>
	<option value="felicitaciones">Felicitaciones</option>
</select>
<br>
<label for="solicitud">Digite su inquetud</label>
<br>
<textarea name="solicitud" id="solicitud" cols="50" rows="5" required></textarea>
<br>
<label for="fechacontacto">Fecha en la cual desea que lo contactemos</label>
<br>
<input type="date" name="fechacontacto" id="fechacontacto" maxlength="8" required>
<br>
<input type="checkbox" name="acepto" id="acepto" value="1" required><label for="acepto">Acepto termino y condiciones</label>
<br>
<button name="enviar" id="enviar" type="submit">Enviar Formulario</button>



</form>

</body>
</html>