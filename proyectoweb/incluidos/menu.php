<?php
/*
Este archivo contiene el menu de la parte superior de la aplicacion
*/
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-info">
  <a class="navbar-brand" href="#">Appweb</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="principal.php">Principal <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="usuarios.php">Usuarios</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="clientes.php">Clientes</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="productos.php">Productos</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="archivos.php">Archivos</a>
      </li>

    </ul>
    <form class="form-inline my-2 my-lg-0">
    	<a href="salir.php" class="btn btn-danger my-2 my-sm-0">Salir</a>
    </form>
  </div>
</nav>