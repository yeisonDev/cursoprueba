<?php
/*
Este include guardara la validacion de acceso al aplicativo que se incluira en cada pagina que lo necesite
*/
session_start();
if (!isset($_SESSION['nombreusuario'])) {
	header("Location: index.php");
	exit();
}

?>