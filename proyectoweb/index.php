<?php
/*
Pagina de logueo al sistema
*/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Acceso Al sistema</title>
<?php include("incluidos/head.php");?>
	</head>
<body>
<h1>Acceso al sistema</h1>
<form name="frmacceso" id="frmacceso" method="post" action="validar.php">
  <div class="form-group row">
    <label for="usuario" class="col-sm-2 col-form-label">Correo electrónico</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="usuario" name="usuario" placeholder="Ingrese su correo " autocomplete="off" required>
    </div>
  </div>
  <div class="form-group row">
    <label for="clave" class="col-sm-2 col-form-label">Contraseña</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="clave" name="clave" placeholder="Contraseña" autocomplete="off" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Ingresar</button>
    </div>
  </div>
</form>
<?php include("incluidos/js.php");?>
</body>
</html>