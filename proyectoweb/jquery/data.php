<?php
/*
Script que recibe las peticiones del ajax o cualquier otra fuente pero con la condicion que lo datos siempre deben ser en metodo post
*/
// proceso de insercion de registros de clientes
// se usara el modelo de  objetos creando una clase que se llame cliente que a su vez
// cargar conexion.php
// esta clase se le invoca el constructor y se le va a crear el metodo ingreso
include("../clases/clientes.php");
$clientes=new Clientes;
$proceso=$clientes->ingreso();
// la variable proceso espera un vector con:
// registro ingresado 
// registro duplicado
// el registro no se puede procesar
// y estas respuestas son las que le devolvemos al ajax
switch ($proceso) {
	case '1':
		echo "<span class='btn btn-success'>El registro ha sido ingresado</span>";
		break;
	case '2':
		echo "<span class='btn btn-warning'>El registro ya se encuentra en el sistema. Intente de nuevo</span>";
		break;
	default:
		echo "<span class='btn btn-danger'>Hay un error en el proceso con la base de datos.</span>";
		break;
}
?>
