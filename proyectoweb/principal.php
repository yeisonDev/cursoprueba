<?php
/*
pagina principal de la aplicacion
Si queremos evitar que alguien entre sin que este logueado
Debemos activar las sessiones y preguntar si la session tiene valores. Si la respuesta es negativa devolverlo a la pagina index.php
La validacion de este acceso siempore se hace al comienzo de cada pagina
Por cada total de registros debemos cargar la clase correspondiente
*/
require("incluidos/sessiones.php");
include_once("clases/clientes.php");


$clientes=new Clientes;
$total_clientes=$clientes->listar();
// si deseamos saber cuantos datos hay en un array usamos sizeof
$total_clientes=sizeof($total_clientes);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Principal del sistema</title>
<?php include("incluidos/head.php");?>

</head>
<body>
<?php include("incluidos/menu.php");?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="#">Principal</a></li>
    <li class="breadcrumb-item active" aria-current="page">Bienvenido, <?php echo $_SESSION['nombreusuario'];?></li>
  </ol>
</nav>

<div class="row">
  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Usuarios <span class="badge badge-info">1</span></h5>
        <p class="card-text">Click para ingresar al módulo de usuarios</p>
        <a href="usuarios.php" class="btn btn-primary">Ir Al módulo</a>
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Clientes <span class="badge badge-info"><?php echo $total_clientes;?></span></h5>
        <p class="card-text">Click para ingresar al módulo de clientes</p>
        <a href="clientes.php" class="btn btn-primary">Ir Al módulo</a>
      </div>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Productos <span class="badge badge-info">2</span></h5>
        <p class="card-text">Click para ingresar al módulo de productos</p>
        <a href="productos.php" class="btn btn-primary">Ir Al módulo</a>
      </div>
    </div>
  </div>

</div>

<?php include("incluidos/js.php");?>

</body>
</html>




