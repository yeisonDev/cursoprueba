<?php
/*
vista central del formulario de clientes que nos sirve tanto 
para ingresar como modificar datsos
todos los procesos se haran por medio de ajax
*/
require("incluidos/sessiones.php");
// si se pasa el parametro para modificar, invocamos la clase y vamos a crear una nueva funcion
// que se llame detalle. la cual nos va a retorna un vector con los campos y sus respectivos valores
// se comporta de manera similar al listar
if (count($_GET)>0) {
  include("clases/productos.php");
  $data=new Productos;
  $resultado=$data->detalle();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registro de datos</title>
<?php include("incluidos/head.php");?>
<link rel="stylesheet" type="text/css" href="pace/pace.css">
<script type="text/javascript" src="pace/pace.min.js"></script>
</head>
<body>
<?php include("incluidos/menu.php");?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="principal.php">Principal</a></li>
    <li class="breadcrumb-item"><a href="clientes.php">Listado de usuarios</a></li>
    <li class="breadcrumb-item active" aria-current="page">Registro de Productos</li>
  </ol>
</nav>
<div class="row">
 <a href="productos.php" class="btn btn-success">Regresar</a>
</div>
<div class="row">
  <div class="col-sm-12">

<form id="frm" name="frm" method="post" action="">
   <div class="form-group row">
      <label for="nombre" class="col-sm-2 col-form-label">Nombres</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre completo " autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["nombre"]?>">
      </div>
    </div>
  <div class="form-group row">
      <label for="referencia" class="col-sm-2 col-form-label">Referencia</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="referencia" name="referencia" placeholder="Referencia " autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["referencia"]?>">
      </div>
    </div>
  <div class="form-group row">
      <label for="valor" class="col-sm-2 col-form-label">Valor</label>
      <div class="col-sm-9">
         <input type="number" class="form-control" id="valor" name="valor" placeholder="Email" autocomplete="off" required value="<?php if (isset($resultado)) echo $resultado["valor"]?>" >
      </div>
    </div>

<?php if (isset($resultado)) {?>
<input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>">
<?php }?>
 <div class="form-group row">
      <div class="col-sm-9">
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </div>
    </div>
  </form>
<div id="mensaje"></div>

</div>
</div>
<?php include("incluidos/js.php");?>
<script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    //invocar el ajax junto con sus parametros
    $("#frm").submit(function(evento){
        evento.preventDefault();
        // para no estar invocando todos los parametros a pasar, usamos serialize()
        // lo que hace es que organiza todos los campos con sus valores en un array de manera
        // automatica
        var parametros=$("#frm").serialize();

        $.ajax({
        data : parametros,
        url: "productos-data.php",
        type: "post",
        beforesend : function () {
            $("#mensaje").html("<span class='btn btn-warning'>Cargando información...</span>");
        },
        success : function (response) {
            $("#mensaje").show();
            $("#mensaje").html(response);
            $("#mensaje").fadeOut(5000);
        },
        error : function (jqXHR,textStatus,errorThrown) {
          $("#mensaje").html("<span class='btn btn-danger'>Se ha presentado un error: "+errorThrown+" : "+textStatus+"</span>");
        } 

        });

    }); 
});   

</script>
</body>
</html>