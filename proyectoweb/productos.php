<?php 
include("incluidos/sessiones.php");
include("clases/productos.php");

$producto= new Productos;

$lista_productos= $producto->listar();
if (count($_GET)>0) {
  // se paso el id que viene del boton eliminar
  $proceso=$producto->eliminar();
  if ($proceso==1) { 
      $mensaje="<div class='alert alert-success'>El registro ha sido eliminado con exito</div>";
  } elseif ($proceso==0) {
      $mensaje="<div class='alert alert-danger'>El proceso no se puede realizar. Intente de nuevo</div>";

  }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Principal del sistema</title>
<?php include("incluidos/head.php");?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="pace/pace.css">
<script type="text/javascript" src="pace/pace.min.js"></script>
</head>
<body>
	<?php include("incluidos/menu.php");?>
	<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Appweb</a></li>
    <li class="breadcrumb-item"><a href="principal.php">Principal</a></li>
    <li class="breadcrumb-item active" aria-current="page">Listado de Productos</li>
  </ol>
</nav>

<?php

if (isset($mensaje)) { ?>
  <div class="row">
    <?php echo $mensaje;?>
  </div>  
<?php
}
?>

<div class="row">
 <a href="form-nuevoProducto.php" class="btn btn-success">Nuevo Registro</a>
</div>
<br>
<div class="row">
  <div class="col-sm-12">
 

  <table class="table" id="data-producto">
    <thead>
      <tr>
        <th>ID</th>
        <th>Referencia</th>
        <th>Nombre</th>
        <th>Valor</th>
        <th>Fecha de ingreso</th>
        <th>Ultima modificacion</th>
        <th>Opciones</th>
      </tr>
    </thead>
<tbody>
  <?php 
    foreach($lista_productos as $vector) {
      ?>
  <tr ">
        <th scope="row"><?php echo $vector["id"];?></th>
        <td><?php echo $vector["referencia"];?></td>
        <td><?php echo $vector["nombre"];?></td>
        <td><?php echo $vector["valor"];?></td>
        <td><?php echo $vector["fechaingreso"];?></td>
        <td><?php echo $vector["fechamodificacion"];?></td>
        <td>
          <a href="productos-form.php?id=<?php echo base64_encode($vector["id"]);?>" class="btn btn-info">Modificar</a>
          <a href="productos.php?id=<?php echo base64_encode($vector["id"]);?>" class="btn btn-danger">Eliminar</a>
        </td>
 </tr>
      <?php
    }?>
</tbody>
  </table>
</div>

</div>

<?php include("incluidos/js.php");?>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	
	$(document).ready( function () {
    $('#data-producto').DataTable(
    {
    	"language": { 
    		//"url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    	}
    }

    	);
    
	} );


</script>
</body>
</html>


</body>
</html>