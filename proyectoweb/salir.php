<?php
/*
Este script destruye las sessiones y lo devuelve a la pagina inde.php
*/
session_start();
session_destroy();
header("Location: index.php");
?>