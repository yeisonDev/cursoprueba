-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2018 at 06:37 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desarrolloweb122018`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblclientes`
--

CREATE TABLE `tblclientes` (
  `id` int(9) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `movil` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `fechaingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='guarda los clientes';

--
-- Dumping data for table `tblclientes`
--

INSERT INTO `tblclientes` (`id`, `nombres`, `apellidos`, `movil`, `correo`, `fechaingreso`, `fechamodificacion`) VALUES
(15, 'juan fernando', 'fernandez', '31202114455', 'juanff11213@g1mail.1com', '2018-10-17 01:10:57', '2018-10-17 01:10:57'),
(18, 'juan fernando f', 'fernandez', '31202114455', 'juanff112123@g1mail.1com', '2018-10-17 01:11:15', '2018-10-17 01:11:15'),
(19, 'juan fernando fg', 'fernandez', '31202114455', 'juanff112163@g1mail.1com', '2018-10-17 01:11:21', '2018-10-17 01:11:21'),
(21, 'juan fernando fg', 'fernandez', '31202114455', 'juanff112193@g1mail.1com', '2018-10-17 01:11:32', '2018-10-17 01:11:32'),
(22, 'juan fernando fg', 'fernandez', '31202114455', 'juanff1121936@g1mail.1com', '2018-10-17 01:11:36', '2018-10-17 01:11:36'),
(24, 'juan fernando fg7', 'fernandez', '31202114455', 'juanff11221936@g1mail.1com', '2018-10-17 01:11:43', '2018-10-17 01:11:43'),
(25, 'juan fernando fg7', 'fernandez', '31202114455', 'juanfer19756@g1mail.1com', '2018-10-17 01:11:58', '2018-10-17 01:11:58'),
(26, 'juan fernando fg7', 'fernandez', '31202114455', 'juanfer19757@g1mail.1com', '2018-10-17 01:12:02', '2018-10-17 01:12:02'),
(27, 'juan fernando fg7', 'fernandez', '31202114455', 'juanfer19@g1mail.1com', '2018-10-17 01:12:06', '2018-10-17 01:12:06'),
(28, 'juan fernando fg7', 'fernandez', '31202114455', 'juanfer29@g1mail.1com', '2018-10-17 01:12:10', '2018-10-17 01:12:10'),
(29, 'juan fernando fg7', 'fernandez', '31202114455', 'hh@hotmail.com', '2018-10-17 01:12:15', '2018-10-17 01:12:15'),
(31, 'juan fernando fg7', 'fernandez', '31202114455', 'hh1@hotmail.com', '2018-10-17 01:12:19', '2018-10-17 01:12:19'),
(32, 'juan fernando fg7', 'fernandez', '31202114455', 'hh2@hotmail.com', '2018-10-17 01:12:21', '2018-10-17 01:12:21'),
(33, 'juan fernando fg7', 'fernandez', '31202114455', 'hh3@hotmail.com', '2018-10-17 01:12:25', '2018-10-17 01:12:25'),
(34, 'juan fernando fg7', 'fernandez', '31202114455', 'hh4@hotmail.com', '2018-10-17 01:12:28', '2018-10-17 01:12:28'),
(35, 'juan fernando fg7', 'fernandez', '31202114455', 'hh5@hotmail.com', '2018-10-17 01:12:32', '2018-10-17 01:12:32'),
(36, 'juan xc', 'ferandnezo', '31200022333', 'juanfxx@gmail.com', '2018-10-31 01:18:14', '2018-10-31 01:18:14'),
(39, 'carlos x', 'carlox y', '1123435353454', 'carlos@gmail.com', '2018-10-31 01:21:23', '2018-10-31 01:21:23'),
(52, 'c', 'c', '1122', 'c@c.fom', '2018-10-31 01:22:19', '2018-10-31 01:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `id` int(8) NOT NULL COMMENT 'clave primaria de usuarios',
  `nombres` varchar(50) NOT NULL COMMENT 'nombres del usuario',
  `apellidos` varchar(50) NOT NULL COMMENT 'apellidos del usuario',
  `correo` varchar(100) NOT NULL COMMENT 'email campo unico',
  `clave` varchar(100) NOT NULL COMMENT 'contraseña encriptada',
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1=Activo; 2=inactivo',
  `fechaingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'fecha de creacion',
  `fechamodificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'fecha de algun cambio'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabla que guarda los usuarios';

--
-- Dumping data for table `tblusuarios`
--

INSERT INTO `tblusuarios` (`id`, `nombres`, `apellidos`, `correo`, `clave`, `activo`, `fechaingreso`, `fechamodificacion`) VALUES
(1, 'JUAN FERNANDO', 'FERNANDEZ ALAVREZ', 'juanff@gmail.com', '$2y$10$KxzwAcwOUPBcehtCt128eum1zTKg/vLV//vNTRFg3umgfyX8lqrfe', 1, '2018-09-19 00:30:36', '2018-09-19 01:03:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblclientes`
--
ALTER TABLE `tblclientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indexes for table `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblclientes`
--
ALTER TABLE `tblclientes`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `tblusuarios`
--
ALTER TABLE `tblusuarios`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'clave primaria de usuarios', AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
