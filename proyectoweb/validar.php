<?php
/*
Script que recibe los datos enviados desde el formulario de index.php
En este script se realiza:
1. recuperacion de las variables
2. conexion a base de datos
3. Validar si las variables del formulario estan con datos o no
4. validar si el usuario que intenta ingresar se encuentra en la tabla
5. si el punto 3 es verdadero pasa a validar el punto 4. Si es falso lo debe devolver a index.php
6. si el punto 4 es verdadero , lo debe dejar pasar a principal.php. Si es falso lo debe devolver a index.php
7. si el punto 4 es verdadero, se deben activar las variables de session para guardar el nombre del usuario y el id para que se puedan usar en todo el proyecto como validacion que se encuentra logueado en el sistema 
*/
if (isset($_POST['usuario']) && isset($_POST['clave'])) 
{ 
	session_start();
	// invocar la clase usuarios
	include("clases/usuarios.php");
	$usuario=new Usuarios;
	$acceso=$usuario->acceso();

	if (count($acceso)>0) {
		// crear las variables de session
		$_SESSION['nombreusuario']=$acceso["nombres"]." ".$acceso["apellidos"];
		$_SESSION['idusuario']=$acceso["id"];
		$_SESSION['correousuario']=$acceso["correo"];
		header("Location: principal.php");
	} else {
		header("Location: index.php");
	}



} else {
	// devolver al index
	header("Location: index.php");
}
?>










